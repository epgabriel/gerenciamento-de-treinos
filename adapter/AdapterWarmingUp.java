package ekelf.bodygymapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.management.BGManager;
import ekelf.bodygymapp.model.exercise.WarmingUp;

public class AdapterWarmingUp extends RecyclerView.Adapter<AdapterWarmingUp.ExerciseViewHolder>{

    private WarmingUp exercise;

    public AdapterWarmingUp(WarmingUp exercise) {
        this.exercise = exercise;
    }

    @NonNull
    @Override
    public ExerciseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemLista = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_aux_exercise, viewGroup, false);
        return new ExerciseViewHolder(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull ExerciseViewHolder exerciseViewHolder, int i) {
        exerciseViewHolder.exercise.setText(exercise.getExercise().getName());
        exerciseViewHolder.timing.setText(String.valueOf(exercise.getTiming()));
        exerciseViewHolder.weight.setText(
                String.format(
                        BGManager.getInstance().getBrazilLocale(),
                        "%.1f",
                        exercise.getSpeed()
                )
        );
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ExerciseViewHolder extends RecyclerView.ViewHolder{

        /** exercise text view*/
        TextView exercise;

        /** series text view */
        TextView timing;

        /** weight text view */
        TextView weight;


        /**
         * Holder to achieve the data retrieved
         * @param itemView
         */
        public ExerciseViewHolder(View itemView) {
            super(itemView);

            exercise = itemView.findViewById(R.id.exercise);
            timing = itemView.findViewById(R.id.timing);
            weight = itemView.findViewById(R.id.weight);
        }
    }

}
