package ekelf.bodygymapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.management.BGManager;
import ekelf.bodygymapp.model.exercise.ExerciseSet;


/**
 * Adapter class. It adapters the Workout Log data retrieved from workout log Recycler View
 *
 * @author Gabriel Edmilson
 * @version 1.0
 * @since 2019-03-05
 */
public class AdapterWorkoutLog extends RecyclerView.Adapter<AdapterWorkoutLog.MyViewHolder> {

    /** OneSet list */
    private List<ExerciseSet> exerciseSetList;


    /**
     * Constructs the Adapter, initializes exercise list
     * @param oneSetList
     */
    public AdapterWorkoutLog(List<ExerciseSet> oneSetList) {
        this.exerciseSetList = oneSetList;
    }


    /**
     * Brings the layout of workout log to the layout in activity
     * @param parent layout
     * @param i position
     * @return Attendance view holder to manipulate the data retrieved
     */
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemLista = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_workout_log, parent, false);
        return new MyViewHolder(itemLista);
    }


    /**
     * Alters the inner data of the workoutLog retrieved
     * @param holder
     * @param i position
     */
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {

        ExerciseSet exerciseSet = exerciseSetList.get(i);

        holder.exercise.setText(exerciseSet.exerciseName());
        holder.series.setText(exerciseSet.getSeries().toString());
        holder.weight.setText(
                String.format(
                        BGManager.getInstance().getBrazilLocale(),
                        "%.0f",
                        exerciseSet.getAverageWeight()
                )
        );
    }

    /**
     * @return amount of items to adapter
     */
    @Override
    public int getItemCount() {
        return exerciseSetList.size();
    }



    /**
     * Inner class, attends and models the data needed in the recycler view
     *
     * @author Gabriel Edmilson
     * @version 1.0
     * @since 2019-03-05
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        /** exercise text view*/
        TextView exercise;

        /** series text view */
        TextView series;

        /** weight text view */
        TextView weight;


        /**
         * Holder to achieve the data retrieved
         * @param itemView
         */
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            exercise = itemView.findViewById(R.id.exercise);
            series = itemView.findViewById(R.id.series);
            weight = itemView.findViewById(R.id.weight);
        }
    }


}
