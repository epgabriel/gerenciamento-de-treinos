package ekelf.bodygymapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.model.exercise.ExerciseSet;


/**
 * Adapter class. It adapters the TrainingModuleType Sheet data retrieved from recycler_training sheet Recycler View
 *
 * @author Gabriel Edmilson
 * @version 1.0
 * @since 2019-03-05
 */
public class AdapterTrainingSheet extends RecyclerView.Adapter<AdapterTrainingSheet.TrainingSheetHolder> {

    /** OneSet list */
    private List<ExerciseSet> exerciseSetList;


    /**
     * Constructs the Adapter, initializes exercise list
     * @param exerciseSetList
     */
    public AdapterTrainingSheet(List<ExerciseSet> exerciseSetList) {
        this.exerciseSetList = exerciseSetList;
    }


    /**
     * Brings the layout of recycler_training sheet to the layout in activity
     * @param parent layout
     * @param i position
     * @return Attendance view holder to manipulate the data retrieved
     */
    @NonNull
    @Override
    public TrainingSheetHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new TrainingSheetHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recycler_training, parent, false)
        );
    }


    /**
     * Alters the inner data of the recycler_training sheet retrieved
     * @param tsHolder
     * @param i position
     */
    @Override
    public void onBindViewHolder(@NonNull TrainingSheetHolder tsHolder, int i) {
        tsHolder.exerciseButton.setText(this.exerciseSetList.get(i).exerciseName());
    }


    /**
     * @return amount of items to adapter
     */
    @Override
    public int getItemCount() {
        return this.exerciseSetList.size();
    }



    /**
     * Inner class, attends and models the data needed in the recycler view
     *
     * @author Gabriel Edmilson
     * @version 1.0
     * @since 2019-03-05
     */
    public class TrainingSheetHolder extends  RecyclerView.ViewHolder {

        /** Holder exercise button */
        private Button exerciseButton;


        /**
         * Holder to achieve the data retrieved
         * @param itemView
         */
        public TrainingSheetHolder(View itemView) {
            super(itemView);
            this.exerciseButton = itemView.findViewById(R.id.training_sheet_training_btn);
        }
    }
}
