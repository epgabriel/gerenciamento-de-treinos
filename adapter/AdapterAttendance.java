package ekelf.bodygymapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.model.Attendance;


/**
 * Adapter class. It adapters the Attendance data retrieved from Attendance Recycler View
 *
 * @author Gabriel Edmilson
 * @version 1.0
 * @since 2019-03-05
 */
public class AdapterAttendance  extends RecyclerView.Adapter<AdapterAttendance.AttendanceViewHolder> {

    /** Attendance list */
    private List<Attendance> attendanceList;


    /**
     * Constructs the Adapter, initializes recycler_attendance list
     * @param attendanceList
     */
    public AdapterAttendance(List<Attendance> attendanceList) {
        this.attendanceList = attendanceList;
    }


    /**
     * Brings the layout of recycler_attendance to the layout in activity
     * @param parent layout
     * @param i position
     * @return Attendance view holder to manipulate the data retrieved
     */
    @NonNull
    @Override
    public AttendanceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new AttendanceViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recycler_attendance, parent, false)
        );
    }


    /**
     * Alters the inner data of the recycler_attendance retrieved
     * @param holder
     * @param i position
     */
    @Override
    public void onBindViewHolder(@NonNull AttendanceViewHolder holder, int i) {
        Attendance attendance = this.attendanceList.get(i);

        holder.type.setText(attendance.getType().name());
        holder.complete_date.setText(attendance.getCompleteDate());
        if (attendance.getCompleteDate() == "-") holder.complete_date.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
    }


    /**
     * @return amount of items to adapter
     */
    @Override
    public int getItemCount() { return this.attendanceList.size(); }


    /**
     * Inner class, attends and models the data needed in the recycler view
     *
     * @author Gabriel Edmilson
     * @version 1.0
     * @since 2019-03-05
     */
    public class AttendanceViewHolder extends RecyclerView.ViewHolder{

        /** Attendance type text view */
        private TextView type;

        /** Attendance completed date text view */
        private TextView complete_date;


        /**
         * Holder to achieve the data retrieved
         * @param itemView
         */
        public AttendanceViewHolder(View itemView) {
            super(itemView);

            this.type = itemView.findViewById(R.id.type);
            this.complete_date = itemView.findViewById(R.id.complete_date);
        }
    }
}
