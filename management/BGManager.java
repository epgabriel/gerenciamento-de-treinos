package ekelf.bodygymapp.management;

import android.content.Context;
import android.net.ConnectivityManager;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ekelf.bodygymapp.R;

/**
 * Manager class of the application. Offers static important methods to the app.
 *
 * @author Gabriel Edmilson Pinto.
 * @version 1.0
 * @since 2019-04-01
 */
public class BGManager {
    private static BGManager instance = null;

    private final String PREFERENCES_FILENAME = "PreferencesFile";

    public BGManager() {}

    public static BGManager getInstance() {
        if (instance == null) return new BGManager();
        return instance;
    }


    public final String getPreferencesFileName() {
        return PREFERENCES_FILENAME;
    }

    /* verifies there is connection with the internet */
    public boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService (context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected())
            return true;
        return false;
    }

    /** @return configured locale to brazil*/
    public Locale getBrazilLocale() {
        if (Locale.getDefault().getCountry() != "Brazil") {
            Locale.setDefault(new Locale("pt", "Brazil"));
            new java.text.DecimalFormat("###,###.##");
        }
        return Locale.getDefault();
    }


    public int todayAbrvID() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());

        int day = cal.get(Calendar.DAY_OF_WEEK);

        if      (day == 2) return R.string.monday_abr;

        else if (day == 3) return R.string.tuesday_abr;

        else if (day == 4) return R.string.wednesday_abr;

        else if (day == 5) return R.string.thursday_abr;

        else if (day == 6) return R.string.friday_abr;

        else if (day == 7) return R.string.saturday_abr;

        else return R.string.sunday_abr;

    }
}
