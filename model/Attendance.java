package ekelf.bodygymapp.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import ekelf.bodygymapp.model.enums.TrainingType;

/**
 * Attendance model class. Which recycler_training type, and which date it was completed
 *
 * @author Gabriel Edmilson
 * @version 1.0
 * @since 2019-03-05
 */
public class Attendance {

    /** TrainingModuleType type */
    private TrainingType type;



    /** TrainingModuleType completeDate */
    private String completeDate;



    /**
     * Constructs an recycler_attendance object
     * @param type
     * @param completeDate
     */
    public Attendance(TrainingType type, Date completeDate) {
        this.type = type;
        this.completeDate = new SimpleDateFormat("dd/MM/yy HH:mm")
                .format(completeDate);
    }



    /**
     * Constructs an recycler_attendance which was'nt completed yet
     * @param type
     */
    public Attendance(TrainingType type) {
        this.type = type;
        this.completeDate = "-";
    }



    /** @return recycler_training's type recycler_attendance */
    public TrainingType getType() { return type; }



    /** @return recycler_training's completed date - if completed */
    public String getCompleteDate() { return completeDate; }



    /** Set recycler_training's type (A, B, C or D) */
    public void setType(TrainingType type) { this.type = type; }



    /** Set recycler_training's complete date */
    public void setCompleteDate(String completeDate) {
        this.completeDate = new SimpleDateFormat("dd/MM/yy HH:mm").format(completeDate);
    }



}
