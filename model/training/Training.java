package ekelf.bodygymapp.model.training;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ekelf.bodygymapp.model.enums.TrainingModuleType;
import ekelf.bodygymapp.model.enums.TrainingType;


/**
 * Training model class.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-04-10
 */
public class Training implements Serializable {

    /** Training Modules (A, B, C, D and/or E)*/
    private List<TrainingModule> trainingModules;

    /** Total days training so far */
    private int trainingDaysCount;

    /** Total days that should be trained in this training */
    private int totalDaysToTrainingComplete;

    /** Version of the Training */
    private int version;

    /** AB, ABC, ABCD, ABCDE or ABCDEF */
    private TrainingModuleType trainingModuleType;


    /**
     * Constructs a new Training object.
     */
    public Training() {
        this.trainingModules = new ArrayList<>();
        this.trainingDaysCount = 0;
        this.totalDaysToTrainingComplete = 0;
        this.version = 0;
        this.trainingModuleType = TrainingModuleType.AB;
    }


    public List<TrainingModule> getTrainingModules() {
        return trainingModules;
    }

    public void setTrainingModules(List<TrainingModule> trainingModules) {
        this.trainingModules = trainingModules;
    }

    public int getTrainingDaysCount() {
        return trainingDaysCount;
    }

    public void setTrainingDaysCount(int trainingDaysCount) {
        this.trainingDaysCount = trainingDaysCount;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public TrainingModuleType getTrainingModuleType() {
        return trainingModuleType;
    }

    public void setTrainingModuleType(TrainingModuleType trainingModuleType) {
        this.trainingModuleType = trainingModuleType;
    }

    public int getTotalDaysToTrainingComplete() {
        return totalDaysToTrainingComplete;
    }

    public void setTotalDaysToTrainingComplete(int totalDaysToTrainingComplete) {
        this.totalDaysToTrainingComplete = totalDaysToTrainingComplete;
    }

    /**
     * Initiates the training modules according to the training type
     */
    public void initiateTrainingModules() {
        int count = this.trainingModuleType.toString().length();

        this.trainingModules = new ArrayList<>();
        for (int i = 0; i < count; i++) this.trainingModules.add(new TrainingModule());
    }

    @Exclude
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        map.put("version", this.version);
        map.put("training_days_count", this.trainingDaysCount);
        map.put("total_days_to_training_complete", this.totalDaysToTrainingComplete);
        map.put("training_module_type", this.trainingModuleType);

        int i = 0;
        for(TrainingModule tm : this.trainingModules)
            map.put(String.format("module_%d",i++), tm.toMap());

        return map;
    }

    @Exclude
    public static Training buildObject(Map<String, Object> map) {

        // Verify if the given map is valid
        boolean validMap = true;
        if ( ! map.containsKey("version") )         validMap = false;
        if ( ! map.containsKey("training_days_count") )  validMap = false;
        if ( ! map.containsKey("total_days_to_training_complete") )   validMap = false;
        if ( ! map.containsKey("training_module_type")) validMap = false;

        // if it is not, then is not possible to build the object.
        if ( ! validMap ) return null;

        Training t = new Training();

        t.setVersion(
                (int) map.get("version")
        );

        t.setTrainingDaysCount(
                (int) map.get("training_days_count")
        );

        t.setTotalDaysToTrainingComplete(
                (int) map.get("total_days_to_training_complete")
        );

        t.setTrainingModuleType(
                (TrainingModuleType) map.get("training_module_type")
        );


        List<TrainingModule> trainingModuleList = new ArrayList<>();
        for(int i = 0; i < t.getTrainingModuleType().toString().length(); i++){
            trainingModuleList.add(
                    TrainingModule.buildObject(
                            TrainingType.valueOf(
                                    String.valueOf(t.getTrainingModuleType().toString().charAt(i))
                            ) ,
                            (Map<String, Object>) map.get(String.format("module_%d", i))
                    )
            );
        }
        t.setTrainingModules(trainingModuleList);

        return t;
    }

}
