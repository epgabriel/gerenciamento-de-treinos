package ekelf.bodygymapp.model.training;

import android.util.Log;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ekelf.bodygymapp.model.enums.TrainingModuleType;
import ekelf.bodygymapp.model.enums.TrainingType;
import ekelf.bodygymapp.model.exercise.ExerciseSet;
import ekelf.bodygymapp.model.exercise.WarmingUp;

/**
 * TrainingModuleType model class. Contains a type (A, B, C or D), a description, a warming-up exercise for
 * starting and for ending the recycler_training, and a list of exercises in between, which models the recycler_training.
 *
 * @author Gabriel Edmilson
 * @version 1.0
 * @since 2019-03-05
 */
public class TrainingModule implements Serializable {

    /** TrainingModuleType type (A, B, C or D)*/
    private TrainingType type;

    /** TrainingModuleType's description */
    private String description;

    /** TrainingModuleType's pre-recycler_training exercise */
    private WarmingUp warmingUp;

    /** TrainingModuleType's exercise list */
    private List<ExerciseSet> exerciseSetList;

    /** TrainingModuleType's pos-recycler_training exercise */
    private WarmingUp posTraining;


    /** Empty Object for building */
    public TrainingModule(){
        this.exerciseSetList = new ArrayList<>();
        this.warmingUp = null;
        this.posTraining = null;
    }

    public TrainingModule(TrainingType type) {
        this.type = type;
        this.exerciseSetList = new ArrayList<>();
        this.warmingUp = null;
        this.posTraining = null;
    }

    /**
     * Constructs a full recycler_training
     * @param type
     * @param description
     * @param warmingUp
     * @param exerciseSetList
     * @param posTraining
     */
    public TrainingModule(TrainingType type, String description, WarmingUp warmingUp, List<ExerciseSet> exerciseSetList, WarmingUp posTraining) {
        this.type         =  type;
        this.description  =  description;
        this.warmingUp    =  warmingUp;
        this.exerciseSetList = exerciseSetList;
        this.posTraining  =  posTraining;
    }


    /**
     * Constructs a recycler_training without pos-recycler_training exercise
     * @param type
     * @param description
     * @param warmingUp
     * @param exerciseSetList
     */
    public TrainingModule(TrainingType type, String description, WarmingUp warmingUp, List<ExerciseSet> exerciseSetList) {
        this.type         =  type;
        this.description  =  description;
        this.warmingUp    =  warmingUp;
        this.exerciseSetList = exerciseSetList;
        this.posTraining  =  new WarmingUp();
    }


    /**
     * Constructs a recycler_training without a pre-recycler_training exercise
     * @param type
     * @param description
     * @param exerciseSetList
     * @param posTraining
     */
    public TrainingModule(TrainingType type, String description, List<ExerciseSet> exerciseSetList, WarmingUp posTraining) {
        this.type         =  type;
        this.description  =  description;
        this.warmingUp    =  new WarmingUp();
        this.exerciseSetList = exerciseSetList;
        this.posTraining  =  posTraining;
    }


    /**
     * Constructs a recycler_training without pre and pro-recycler_training exercises
     * @param type
     * @param description
     * @param exerciseSetList
     */
    public TrainingModule(TrainingType type, String description, List<ExerciseSet> exerciseSetList) {
        this.type         =  type;
        this.description  =  description;
        this.warmingUp    =  new WarmingUp();
        this.exerciseSetList = exerciseSetList;
        this.posTraining  =  new WarmingUp();
    }




    /** @return TrainingModuleType's type (A, B, C or D) */
    public TrainingType getType() { return type;  }

    /** @return TrainingModuleType's short description */
    public String getDescription() { return description; }

    /** @return Pre-recycler_training exercise*/
    public WarmingUp getWarmingUp() {return warmingUp; }

    /** @return TrainingModuleType's exercise list */
    public List<ExerciseSet> getExerciseSetList() { return exerciseSetList; }

    /** @return Pro-recycler_training exercise */
    public WarmingUp getPosTraining() { return this.posTraining; }


    /** Set recycler_training's type (A, B, C or D) */
    public void setType(TrainingType type) { this.type = type; }

    /** Set recycler_training's short description */
    public void setDescription(String description) { this.description = description; }

    /** Set pre-recycler_training exercise */
    public void setWarmingUp(WarmingUp warmingUp) { this.warmingUp = warmingUp; }

    /** Set recycler_training's exercise list */
    public void setExerciseSetList(List<ExerciseSet> exerciseSetList) { this.exerciseSetList = exerciseSetList; }

    /** Set pos-recycler_training exercise */
    public void setPosTraining(WarmingUp posTraining) { this.posTraining = posTraining; }


    /**
     * Converts the model to a map for database CRUD.
     * @return
     */
    @Exclude
    public Map<String, Object> toMap() {

        //puts all exercises maps into this map.
        Map<String, Object> exerciseListMap = new HashMap<>();

        int i = 0; // exercises id.
        for (ExerciseSet exerciseSet : this.exerciseSetList) exerciseListMap.put(String.valueOf(i++), exerciseSet.toMap());

        //body map
        Map<String, Object> body = new HashMap<>();
        body.put("type", this.type);
        body.put("description", this.description);          // Training module description
        if (this.warmingUp != null) body.put("warming_up", this.warmingUp.toMap());     // Training warming up exercise
        body.put("exercise_list", exerciseListMap);         // Training exercises
        if (this.posTraining != null) body.put("pos_training", this.posTraining.toMap()); // Pos-training exercises

        return body;
    }

    @Exclude
    public static TrainingModule buildObject(TrainingType TT, Map<String, Object> map) {
        TrainingModule tm = new TrainingModule();

        return tm;
    }
}
