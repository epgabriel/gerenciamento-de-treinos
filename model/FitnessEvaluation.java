package ekelf.bodygymapp.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

import ekelf.bodygymapp.management.BGManager;

/**
 * Fitness evaluation class.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-14
 */
public class FitnessEvaluation {

    /* Measurements */
    private float weight, goal, height, fatRate,
        rightArm, rightForearm, rightLeg, rightCalf,
        leftArm, leftForearm, leftLeg, leftCalf,
        shoulder, chest, hip, waist;

    private int version;

    /**
     * Constructs an empty fitness evaluation object.
     */
    public FitnessEvaluation() {
        this.version = 0;

        this.weight = 0;
        this.goal = 0.0f;
        this.height = 0.0f;
        this.fatRate = 0.0f;

        this.rightArm = 0.0f;
        this.rightForearm = 0.0f;
        this.rightLeg = 0.0f;
        this.rightCalf = 0.0f;

        this.leftArm = 0.0f;
        this.leftForearm = 0.0f;
        this.leftLeg = 0.0f;
        this.leftCalf = 0.0f;

        this.shoulder = 0.0f;
        this.chest = 0.0f;
        this.hip = 0.0f;
        this.waist = 0.0f;
    }



    /**
     * Constructs an entire Fitness Evaluation object.
     *
     * @param weight
     * @param goal
     * @param height
     * @param fatRate
     * @param rightArm
     * @param rightForearm
     * @param rightLeg
     * @param rightCalf
     * @param leftArm
     * @param leftForearm
     * @param leftLeg
     * @param leftCalf
     * @param shoulder
     * @param chest
     * @param hip
     * @param waist
     */
    public FitnessEvaluation(
            float weight,
            float goal,
            float height,
            float fatRate,
            float rightArm,
            float rightForearm,
            float rightLeg,
            float rightCalf,
            float leftArm,
            float leftForearm,
            float leftLeg,
            float leftCalf,
            float shoulder,
            float chest,
            float hip,
            float waist
    ) {
        this.weight = weight;
        this.goal = goal;
        this.height = height;
        this.fatRate = fatRate;
        this.rightArm = rightArm;
        this.rightForearm = rightForearm;
        this.rightLeg = rightLeg;
        this.rightCalf = rightCalf;
        this.leftArm = leftArm;
        this.leftForearm = leftForearm;
        this.leftLeg = leftLeg;
        this.leftCalf = leftCalf;
        this.shoulder = shoulder;
        this.chest = chest;
        this.hip = hip;
        this.waist = waist;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getGoal() {
        return goal;
    }

    public void setGoal(float goal) {
        this.goal = goal;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getFatRate() {
        return fatRate;
    }

    public void setFatRate(float fatRate) {
        this.fatRate = fatRate;
    }

    public float getRightArm() {
        return rightArm;
    }

    public void setRightArm(float rightArm) {
        this.rightArm = rightArm;
    }

    public float getRightForearm() {
        return rightForearm;
    }

    public void setRightForearm(float rightForearm) {
        this.rightForearm = rightForearm;
    }

    public float getRightLeg() {
        return rightLeg;
    }

    public void setRightLeg(float rightLeg) {
        this.rightLeg = rightLeg;
    }

    public float getRightCalf() {
        return rightCalf;
    }

    public void setRightCalf(float rightCalf) {
        this.rightCalf = rightCalf;
    }

    public float getLeftArm() {
        return leftArm;
    }

    public void setLeftArm(float leftArm) {
        this.leftArm = leftArm;
    }

    public float getLeftForearm() {
        return leftForearm;
    }

    public void setLeftForearm(float leftForearm) {
        this.leftForearm = leftForearm;
    }

    public float getLeftLeg() {
        return leftLeg;
    }

    public void setLeftLeg(float leftLeg) {
        this.leftLeg = leftLeg;
    }

    public float getLeftCalf() {
        return leftCalf;
    }

    public void setLeftCalf(float leftCalf) {
        this.leftCalf = leftCalf;
    }

    public float getShoulder() {
        return shoulder;
    }

    public void setShoulder(float shoulder) {
        this.shoulder = shoulder;
    }

    public float getChest() {
        return chest;
    }

    public void setChest(float chest) {
        this.chest = chest;
    }

    public float getHip() {
        return hip;
    }

    public void setHip(float hip) {
        this.hip = hip;
    }

    public float getWaist() {
        return waist;
    }

    public void setWaist(float waist) {
        this.waist = waist;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    /** @return Right arm formatted to string. */
    public String getRightArmFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.rightArm
        );
    }

    /** @return Left arm formatted to string. */
    public String getLeftArmFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.leftArm
        );
    }

    /** @return Right forearm formatted to string. */
    public String getRightForearmFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.rightForearm
        );
    }

    /** @return Left forearm formatted to string. */
    public String getLeftForearmFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.leftForearm
        );
    }

    /** @return Right Leg formatted to string. */
    public String getRightLegFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.rightLeg
        );
    }

    /** @return Left leg formatted to string. */
    public String getLeftLegFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.leftLeg
        );
    }

    /** @return Right calf formatted to string. */
    public String getRightCalfFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.rightCalf
        );
    }

    /** @return Left calf formatted to string. */
    public String getLeftCalfFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.leftCalf
        );
    }

    /** @return Shoulder formatted to string. */
    public String getShoulderFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.shoulder
        );
    }

    /** @return Chest formatted to string. */
    public String getChestFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.chest
        );
    }

    /** @return Hip formatted to string. */
    public String getHipFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.hip
        );
    }

    /** @return Waist formatted to string. */
    public String getWaistFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.waist
        );
    }

    /** @return Amount of weight to lose or gain in order to achieve goal weight. Always positive. */
    public float getPoundsToGoal() {
        return (this.weight - this.goal) < 0
                ? this.goal - this.weight
                : this.weight - this.goal;
    }

    /** @return BMI, which is the current weight divided by the squad of height (in meters). */
    public double getBMI() {
        return this.weight / Math.pow(this.height * 0.01, 2);
    }

    /** @return String BMI formatted to 2 points float. */
    public String getBMIFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                getBMI()
        );
    }

    /** @return Current weight formatted to string. */
    public String getWeightFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.weight
        );
    }

    /** @return Goal weight formatted to string. */
    public String getGoalFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.goal
        );
    }

    /** @return Pounds to goal formatted to string. */
    public String getPoundsToGoalFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                getPoundsToGoal()
        );
    }

    /** @return Fat rate formatted to string. */
    public String getFatRateFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                this.fatRate
        );
    }

    /** @return ideal rate in the format: minimum ideal ~  maximum ideal*/
    public String getIdealWeightFormatted() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f ~ %.1f",
                18.5 * Math.pow(this.height * 0.01, 2),
                25 * Math.pow(this.height * 0.01, 2)
        );

    }

    @Override
    public String toString() {
        return String.format(
                BGManager.getInstance().getBrazilLocale(),
                "\n\n:: Avaliação Nº %d\n" +
                "\tPeso: %.2f\n" +
                "\tObjetivo: %.2f\n" +
                "\tAltura: %.2f\n" +
                "\tIMC: %.2f\n" +
                "\tPeso Ideal: %s\n" +
                "\tTaxa de Gordura Corporal: %.2f\n" +
                "\n:: Medidas\n" +
                "\tBraço (D): %.2f \t\t Braço (E): %.2f\n" +
                "\tAntebraço (D): %.2f \t Antebraço (E): %.2f\n" +
                "\tPerna (D): %.2f \t\t Perna (E): %.2f\n" +
                "\tPanturrilha (D): %.2f \t Panturrilha (E): %.2f\n" +
                "\tOmbros: %.2f\n" +
                "\tPeitoral: %.2f\n" +
                "\tQuadril: %.2f\n" +
                "\tCintura: %.2f\n\n",
                version,
                weight,
                goal,
                height,
                getBMI(),
                getIdealWeightFormatted(),
                fatRate,
                rightArm, leftArm,
                rightForearm, leftForearm,
                rightLeg, leftLeg,
                rightCalf, leftCalf,
                shoulder,
                chest,
                hip,
                waist
        );
    }


    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("version", version);
        result.put("weight", weight);
        result.put("goal", goal);
        result.put("height", height);
        result.put("fatRate", fatRate);
        result.put("rightArm", rightArm);
        result.put("rightForearm", rightForearm);
        result.put("rightLeg", rightLeg);
        result.put("rightCalf", rightCalf);
        result.put("leftArm", leftArm);
        result.put("leftForearm", leftForearm);
        result.put("leftLeg", leftLeg);
        result.put("leftCalf", leftCalf);
        result.put("shoulder", shoulder);
        result.put("chest", chest);
        result.put("hip", hip);
        result.put("waist", waist);
        return result;
    }
}
