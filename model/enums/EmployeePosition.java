package ekelf.bodygymapp.model.enums;

/**
 * Employee position enum class.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-14
 */
public enum EmployeePosition {
    TRAINER { @Override public String toString() { return "Professor"; } },
    INTERN  { @Override public String toString() { return "Estagiário"; } }
}
