package ekelf.bodygymapp.model.enums;

/**
 * Gender enum class.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-14
 */
public enum Gender {
    F { @Override public String toString() { return "F"; } },
    M { @Override public String toString() { return "M"; } },
    O { @Override public String toString() { return "O"; } }
}
