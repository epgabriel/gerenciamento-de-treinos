package ekelf.bodygymapp.model.enums;

/**
 * TrainingModuleType module enum class.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-04-10
 */
public enum TrainingModuleType {
    AB      { @Override public String toString() { return "AB";     } },
    ABC     { @Override public String toString() { return "ABC";    } },
    ABCD    { @Override public String toString() { return "ABCD";   } },
    ABCDE   { @Override public String toString() { return "ABCDE";  } },
    ABCDEF  { @Override public String toString() { return "ABCDEF"; } }
}
