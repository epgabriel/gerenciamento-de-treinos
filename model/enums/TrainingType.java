package ekelf.bodygymapp.model.enums;

/**
 * TrainingModuleType type enum class.
 *
 * @author Gabriel Edmilson
 * @version 1.0
 * @since 2019-03-05
 */
public enum TrainingType {
    A { @Override public String toString() { return "A"; } },
    B { @Override public String toString() { return "B"; } },
    C { @Override public String toString() { return "C"; } },
    D { @Override public String toString() { return "D"; } },
    E { @Override public String toString() { return "E"; } },
    F { @Override public String toString() { return "F"; } }
}
