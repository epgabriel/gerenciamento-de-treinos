package ekelf.bodygymapp.model.enums;

/**
 * Permissions enum class.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-14
 */
public enum Permission {
    ADMIN   { @Override public String toString() { return "Admin"; } },
    TRAINER { @Override public String toString() { return "Professor"; } },
    USER    { @Override public String toString() { return "Usuário"; } }
}
