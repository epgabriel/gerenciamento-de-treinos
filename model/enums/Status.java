package ekelf.bodygymapp.model.enums;

/**
 * Users status enum class.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-14
 */
public enum Status {
    ACTIVE   { @Override public String toString() { return "Ativo"; } },
    INACTIVE { @Override public String toString() { return "Inativo"; } },
    VOID     { @Override public String toString() { return "Outro"; } }
}
