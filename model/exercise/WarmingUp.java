package ekelf.bodygymapp.model.exercise;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

import java.math.BigDecimal;
import java.util.Map;

import ekelf.bodygymapp.R;

/**
 * This class models an Warming-Up exercise, it can be either a pre or pos recycler_training.
 * Extends the OneSet model class.
 *
 * @author  Gabriel Edmilson
 * @version 1.0
 * @since   2019-03-05
 */
public class WarmingUp extends ExerciseSet {

    private float speed;

    /**
     * Constructs an Warming-Up OneSet
     * @param name name of the exercise
     * @param timing timing of the exercise, in minutes
     */
    public WarmingUp(String name, int timing) {
        super();
        super.getExercise(0).setName(name);
        super.setSeries(new Series(1, 1));
        super.setRest_timing(timing);
        this.speed = 0;
    }


    /**
     * Constructs an Warming-Up OneSet
     * @param name name of the exercise
     * @param timing timing of the exercise, in minutes
     * @param speed speed, load or weight of the exercise
     */
    public WarmingUp(String name, int timing, float speed) {
        super();
        super.getExercise(0).setName(name);
        super.setSeries(new Series(1, 1));
        super.setRest_timing(timing);
        this.speed = speed;
    }


    /**
     * Constructs an empty OneSet, it means it has no attribute.
     * The name stands with "-", and all the other attributes are null value (0)
     */
    public WarmingUp(){
        super();
        this.speed  = 0;
    }



    /** @return warming-up's timing */
    public int getTiming() { return super.getRest_timing(); }

    /** @return warming-up's speed or load */
    public float getSpeed()  { return this.speed;  }


    /** Set exercise's timing */
    public void setTiming(int timing) { super.setRest_timing(timing); }

    /** Set exercise's speed or load */
    public void setSpeed(float speed) { this.speed = speed; }



    /**
     * Overrides the toString method. Returns the Warming-Up class in a String format.
     * @return string: Name of the exercise: timing in minutes, load
     */
    public String toString(Context context) {

        String str = "";

        /* If Warming-Up's name is empty, so there is no Warming-Up to describe, return "-" */
        if (super.getExercise(0).getName().isEmpty()) return "-";

        /* Else, the Warming-Up starts with its name*/
        str += super.getExercise(0).getName();

        /* Then, verifies if the timing and speed are not null, put them in the string */

        /* If the timing is not null, then appends it to the string */
        if (super.getRest_timing() > (short) 0)
            str += ": " + super.getRest_timing() + " " + context.getResources().getString(R.string.wklog_timing_description);

        /* If the speed is not null, then appends it to the string */
        if (this.speed  > (short) 0)
            str += ", " + context.getResources().getString(R.string.wklog_load) + " " + this.speed;

        /* Returns the string, formatted in "Name - timing, load." */
        return str;
    }


    // ::::::::::::::::::: Parcelable methods :::::::::::::::::::

    private WarmingUp (Parcel in) {
        super(in);
        this.speed = in.readFloat();
    }

    public static final Parcelable.Creator<WarmingUp> CREATOR = new Parcelable.Creator<WarmingUp>() {
        public WarmingUp createFromParcel(Parcel in) {
            return new WarmingUp(in);
        }

        public WarmingUp[] newArray(int size) {
            return new WarmingUp[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeFloat(this.speed);
    }



    /**
     * Convert the object to a hash map
     * @return
     */
    @Exclude
    @Override
    public Map<String, Object> toMap() {
        Map<String, Object> map = super.toMap();
        map.put("speed", this.speed);
        return map;
    }

    /**
     * Converts a map in a Warming Up object
     * @param map to be converted into a Warming Up object
     * @return WarmingUp object
     */
    @Exclude
    public static WarmingUp buildFromMap( Map<String, Object> map ) {
        if ( map == null ) return new WarmingUp();

        WarmingUp warmingUp = new WarmingUp();
        if ( map.containsKey("speed") ) warmingUp.setSpeed(Float.parseFloat(String.valueOf(map.get("speed"))));
        if ( map.containsKey("sets") && map.containsKey("repeat") ) {
            warmingUp.getSeries().setSets( Integer.parseInt(String.valueOf(map.get("sets"))) );
            warmingUp.getSeries().setRepeat( Integer.parseInt(String.valueOf(map.get("repeat"))) );
        }
        if ( map.containsKey("rest_timing") ) warmingUp.setRest_timing( Integer.parseInt( String.valueOf(map.get("rest_timing") ) ) );
        if ( map.containsKey("name0") ) warmingUp.getExercise().setName( String.valueOf(map.get("name0")) );
        return warmingUp;
    }

}