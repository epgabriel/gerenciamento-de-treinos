package ekelf.bodygymapp.model.exercise;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Serie model. Every exercise has it. It's made of sets and repeats
 *
 * @author Gabriel Edmilson
 * @version 1.0
 * @since 2019-03-05
 */
public class Series implements Parcelable {

    /** Serie's sets */
    private int sets;

    /** Serie's repeats for each set */
    private int repeat;

    /**
     * Constructs an empty serie, it has no sets, and no repeats.
     * It's called for warming-up exercises.
     */
    public Series(){
        this.sets = 0;
        this.repeat = 0;
    }


    /**
     * Constructs an series object.
     * It's called by exercises class.
     * @param sets
     * @param repeat
     */
    public Series(int sets, int repeat) {
        this.sets = sets;
        this.repeat = repeat;
    }


    /** @return Serie's sets */
    public int getSets() { return sets; }

    /** @return Serie's repeats for each set */
    public int getRepeat() { return repeat; }

    /** Set Serie's sets */
    public void setSets(int sets) { this.sets = sets; }

    /** Set Serie's repeat for each set */
    public void setRepeat(int repeat) { this.repeat = repeat; }


    /**
     * Formats the serie's object into a string
     * @return "Sets x Repeats"
     */
    @Override
    public String toString() {
        return String.format("%dx%d", this.sets, this.repeat);
    }


    // ::::::::::::::::::: Parcelable methods :::::::::::::::::::

    private Series (Parcel in) {
        this.sets = in.readInt();
        this.repeat = in.readInt();
    }

    public static final Parcelable.Creator<Series> CREATOR = new Parcelable.Creator<Series>() {
        public Series createFromParcel(Parcel in) {
            return new Series(in);
        }

        public Series[] newArray(int size) {
            return new Series[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(sets);
        dest.writeInt(repeat);
    }



    // ::::::::::::::::::: Excluded methods :::::::::::::::::::

    /**
     * Convert the object into a map, for database CRUD procedures.
     * @return
     */
    @Exclude
    public Map<String, Integer> toMap() {
        Map<String, Integer> map = new HashMap<>();

        map.put("sets", this.sets);
        map.put("repeat", this.repeat);

        return map;
    }


    /**
     * Builds a new Series Object from a map.
     * @param map
     * @return
     */
    @Exclude
    public static Series buildObject (Map<String, Integer> map) {
        // Verify if the given map is valid
        boolean validMap = true;
        if ( ! map.containsKey("sets") )    validMap = false;
        if ( ! map.containsKey("repeat") )  validMap = false;

        // if it is not, then is not possible to build the object.
        if ( ! validMap ) return null;

        Series series = new Series(); // new object.
        series.setSets(map.get("sets"));
        series.setRepeat(map.get("repeat"));

        return series;
    }

}
