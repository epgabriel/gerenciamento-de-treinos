package ekelf.bodygymapp.model.exercise;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Basic exercises are the abstraction of a exercise, with basic information.
 * It represents the exercise method without sets.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-05-15
 */
public class Exercise implements Parcelable{
    private int weight;
    private int dropSet_rate;
    private String name;


    /** Constructor for basic exercises */
    public Exercise(
            String name,
            int weight,
            int dropSet_rate
    ) {
        this.name = name;
        this.weight = weight;
        this.dropSet_rate = dropSet_rate;
    }

    public Exercise() {
        this.name = new String();
        this.weight = 0;
        this.dropSet_rate = 0;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getDropSet_rate() {
        return dropSet_rate;
    }

    public void setDropSet_rate(int dropSet_rate) {
        this.dropSet_rate = dropSet_rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // ::::::::::::::::::: Parcelable methods :::::::::::::::::::

    private Exercise(Parcel in) {
        this.name   = in.readString();
        this.weight = in.readInt();
        this.dropSet_rate = in.readInt();
    }

    public static final Parcelable.Creator<Exercise> CREATOR = new Parcelable.Creator<Exercise>() {
        public Exercise createFromParcel(Parcel in) {
            return new Exercise(in);
        }
        public Exercise[] newArray(int size) {
            return new Exercise[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(weight);
        dest.writeInt(dropSet_rate);
    }

    // ::::::::::::::::::: Excluded methods :::::::::::::::::::

    @Exclude
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", this.name);
        map.put("weight", this.weight);
        map.put("dropset", this.dropSet_rate);
        return map;
    }

    @Exclude
    public Map<String, Object> toMap(int id) {
        Map<String, Object> map = new HashMap<>();
        map.put("name"+String.valueOf(id), this.name);
        map.put("weight"+String.valueOf(id), this.weight);
        map.put("dropset"+String.valueOf(id), this.dropSet_rate);
        return map;
    }


}
