package ekelf.bodygymapp.model.exercise;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class ExerciseSet implements Parcelable {

    /** OneSet's serie (Sets x Repeats) */
    private Series series;

    /** Timing, in minutes, for rest between exercises */
    private int rest_timing;

    protected int size;

    private Exercise[] exercises;

    @Exclude
    private String name;


    public ExerciseSet(Series series, int rest_timing, int size) {
        this.series = series;
        this.rest_timing = rest_timing;
        this.size = size;
        this.exercises = new Exercise[size];
        for (int i = 0; i < size; i++) this.exercises[i] = new Exercise();
    }

    public ExerciseSet(int size) {
        this.series = new Series(0,0);
        this.rest_timing = 0;
        this.exercises = new Exercise[size];
        this.size = size;
        for (int i = 0; i < size; i++) this.exercises[i] = new Exercise();
    }

    public ExerciseSet(
            String name,
            int sets,
            int rept,
            int weight,
            int rest_timing
    ) {
        this.series = new Series(sets, rept);
        this.rest_timing = rest_timing;
        this.size = 1;
        this.exercises = new Exercise[1];

        this.exercises[0] = new Exercise();
        this.exercises[0].setName(name);
        this.exercises[0].setWeight(weight);
    }


    public ExerciseSet() {
        this.series = new Series(0,0);
        this.rest_timing = 0;
        this.exercises = new Exercise[1];
        this.exercises[0] = new Exercise();
        this.size = 1;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    public int getRest_timing() {
        return rest_timing;
    }

    public void setRest_timing(int rest_timing) {
        this.rest_timing = rest_timing;
    }

    public Exercise[] getExercises() {
        return exercises;
    }

    public void setExercises(Exercise[] exercises) {
        this.exercises = exercises;
    }

    public void setExercise(final int pos, Exercise exercise) {
        try {
            this.exercises[pos] = exercise;
        }catch (NullPointerException e){
            Log.e("error", e.toString());
        }
    }

    public Exercise getExercise(int pos) {
        if (pos < 0 || pos > 2) {
            return null;
        }
        return this.exercises[pos];
    }

    public Exercise getExercise() { return this.exercises[0]; }


    public String exerciseName() {
        String s = "";
        for (Exercise exercise : this.exercises) s += " & " + exercise.getName();
        return s.substring(3);
    }


    public double getAverageWeight() {

        if (exercises.length == 0) return 0.0;

        double sum = 0;
        for (Exercise exercise : exercises) sum += exercise.getWeight();
        return sum / exercises.length;
    }


    // ::::::::::::::::::: Parcelable methods :::::::::::::::::::

    protected ExerciseSet (Parcel in) {

        this.series = in.readParcelable(Series.class.getClassLoader());
        this.rest_timing = in.readInt();
        this.size = in.readInt();

        this.exercises = new Exercise[size];

        //this.exercises = (Exercise[]) in.readArray(Exercise.class.getClassLoader());

        for(int i = 0; i < this.size; i++)
            this.exercises[i] = in.readParcelable(Exercise.class.getClassLoader());
    }

    public static final Parcelable.Creator<ExerciseSet> CREATOR = new Parcelable.Creator<ExerciseSet>() {
        public ExerciseSet createFromParcel(Parcel in) {
            return new ExerciseSet(in);
        }

        public ExerciseSet[] newArray(int size) {
            return new ExerciseSet[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(series, flags);
        dest.writeInt(rest_timing);
        dest.writeInt(size);
        //dest.writeArray(exercises);
        for(Exercise exercise : exercises)
            dest.writeParcelable(exercise, flags);
    }

    // ::::::::::::::::::: Exclude methods :::::::::::::::::::
    @Exclude
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();

        map.putAll(series.toMap());
        map.put("rest_timing", rest_timing);

        // Put basic exercises
        for(int i = 0; i < exercises.length; i++)
            map.putAll(exercises[i].toMap(i));

        return map;
    }

    public static ExerciseSet buildFromMap ( Map<String, Object> map ) {

        if (map == null) return new ExerciseSet();
        if (map.keySet().size() % 3 != 0) return new ExerciseSet();

        // key set size must be 6 (OneSet), 9 (BiSet) or 12 (TriSet), and therefore
        // the formula size / 3 will result on 2, 3 or 4.
        // 2, 3 or 4 minus 1 will be 1, 2 or 3 (the number of Exercises)
        ExerciseSet exerciseSet = new ExerciseSet( (map.keySet().size() / 3) - 1 );

        Log.i("size", String.valueOf(map.keySet().size()));
        Log.i("keysetMap", map.toString());

        if (map.containsKey("sets"))   exerciseSet.getSeries().setSets(Integer.parseInt(String.valueOf(map.get("sets"))));
        if (map.containsKey("repeat")) exerciseSet.getSeries().setRepeat(Integer.parseInt(String.valueOf(map.get("repeat"))));
        if (map.containsKey("rest_timing")) exerciseSet.setRest_timing(Integer.parseInt(String.valueOf(map.get("rest_timing"))));

        if (map.containsKey("name0") && map.containsKey("weight0") && map.containsKey("dropset0")) {
            exerciseSet.getExercise(0).setName( String.valueOf( map.get("name0")) );
            exerciseSet.getExercise(0).setWeight( Integer.parseInt(String.valueOf( map.get("weight0"))) );
            exerciseSet.getExercise(0).setDropSet_rate( Integer.parseInt(String.valueOf( map.get("dropset0"))));
        }

        if (map.containsKey("name1") && map.containsKey("weight1") && map.containsKey("dropset1")) {
            exerciseSet.getExercise(1).setName( String.valueOf( map.get("name1")) );
            exerciseSet.getExercise(1).setWeight( Integer.parseInt(String.valueOf( map.get("weight1"))) );
            exerciseSet.getExercise(1).setDropSet_rate( Integer.parseInt(String.valueOf( map.get("dropset1"))));
        }

        if (map.containsKey("name2") && map.containsKey("weight2") && map.containsKey("dropset2")) {
            exerciseSet.getExercise(2).setName( String.valueOf( map.get("name2")) );
            exerciseSet.getExercise(2).setWeight( Integer.parseInt(String.valueOf( map.get("weight2"))) );
            exerciseSet.getExercise(2).setDropSet_rate( Integer.parseInt(String.valueOf( map.get("dropset2"))));
        }

        return exerciseSet;
    }





}
