package ekelf.bodygymapp.model.actors;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ekelf.bodygymapp.model.enums.EmployeePosition;
import ekelf.bodygymapp.model.enums.Gender;
import ekelf.bodygymapp.model.enums.Permission;
import ekelf.bodygymapp.model.enums.Status;

/**
 * Trainer model class
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-14
 */
public class Trainer extends User{

    private List<Boolean> shifts;
    private EmployeePosition position;

    public Trainer(Permission permission) { super(permission); }

    /**
     * Constructs Trainer object with Admin attributes.
     *
     * @param registration
     * @param name
     * @param email
     * @param permission
     */
    public Trainer (
            long registration,
            String name,
            String email,
            Permission permission
    ) {
        super(registration, name, email, permission);
        super.setTrainer_regs(registration);
        this.shifts = new ArrayList<>();
        for (int i = 0; i < 3; i ++) this.shifts.add(false);
    }

    /**
     * Constructs an empty Trainer object
     */
    public Trainer() {
        super(Permission.TRAINER);
        this.shifts = new ArrayList<>();
        for (int i = 0; i < 3; i ++) this.shifts.add(false);
    }

    /**
     * Constructs a full Trainer Object
     * @param registration
     * @param name
     * @param gender
     * @param position
     * @param status
     */
    public Trainer(long registration, String name, String email, Gender gender, EmployeePosition position, Status status) {
        super(registration, name, email, gender, status, Permission.TRAINER);
        super.setTrainer_regs(registration);
        this.position = position;
        this.shifts = new ArrayList<>();
        for (int i = 0; i < 3; i ++) this.shifts.add(false);
    }


    public EmployeePosition getPosition() { return position; }

    public void setPosition(EmployeePosition position) { this.position = position; }

    public void setShift(int id, boolean value) {
        this.shifts.set(id, value);
    }

    public boolean getShift(int id) { return this.shifts.get(id); }


    @Override
    public String toString() {
        String str =String.format(
                "\nNome: %s\t Crachá: %d\nSexo: %s\tSituação: %s\nTurno(s): ",
                super.getName(), super.getRegistration(), super.getGender().toString(), super.getStatus().toString());

        if (this.shifts.get(0)) str += "Manhã\t";
        if (this.shifts.get(1)) str += "Tarde\t";
        if (this.shifts.get(2)) str += "Noite\t";

        str += "\n";

        str += "Posição: " + this.position.toString();

        return str;
    }

    @Exclude
    public Map<String, Object> toMap() {
        Map<String, Object> result = super.toMap();
        result.put("shifts", this.shifts);
        result.put("position", this.position);
        return result;
    }

}
