package ekelf.bodygymapp.model.actors;

import com.google.firebase.database.Exclude;

import java.util.Map;

import ekelf.bodygymapp.model.enums.Permission;

/**
 * Admin model class
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-14
 */
public class Admin extends Trainer {

    /**
     * Constructs an empty Admin object
     */
    public Admin() { super(Permission.ADMIN); }

    /**
     * Constructs a full Admin object
     * @param registration
     * @param name
     * @param email
     */
    public Admin(long registration, String name, String email) {
        super(registration, name, email, Permission.ADMIN);
        super.setTrainer_regs(registration);
    }

    @Exclude
    public Map<String, Object> toMap() {
        Map<String, Object> result = super.toMap();
        result.remove("shifts");
        return result;
    }

}
