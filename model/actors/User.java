package ekelf.bodygymapp.model.actors;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ekelf.bodygymapp.model.enums.Gender;
import ekelf.bodygymapp.model.enums.Permission;
import ekelf.bodygymapp.model.enums.Status;

/**
 * User model class
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-14
 */

public class User implements Serializable {
    private String key;
    private String name;
    private String email;
    private String fitness_ev_expiration;
    private String medical_ex_expiration;
    private String regs_date;
    private Status status;
    private Gender gender;
    private long registration;
    private long trainer_regs;
    private Permission permission;

    /**
     * Constructs an empty User object
     */
    public User() { this.permission = Permission.USER; }

    public User(Permission permission) { this.permission = permission; }

    /**
     * Constructs User object with Admin attributes.
     *
     * @param registration
     * @param name
     * @param email
     * @param permission
     */
    public User (
            long registration,
            String name,
            String email,
            Permission permission
    ) {
        this.registration = registration;
        this.name = name;
        this.email = email;
        this.permission = permission;
    }


    /**
     * Constructs User with Trainer attributes.
     *
     * @param registration
     * @param name
     * @param email
     * @param gender
     * @param status
     * @param permission
     */
    public User (
            long registration,
            String name,
            String email,
            Gender gender,
            Status status,
            Permission permission
    ) {
        this.registration = registration;
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.status = status;
        this.permission = permission;
    }


    /**
     * Constructs a User with basic information only.
     *
     * @param registration
     * @param name
     * @param gender
     * @param status
     */
    public User(
            long registration,
            String name,
            String email,
            Gender gender,
            Status status
    ) {
        this.registration = registration;
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.status = status;
        this.permission = Permission.USER;
    }

    /**
     * Constructs a User with all information.
     *
     * @param registration
     * @param name
     * @param gender
     * @param status
     * @param fitness_ev_expiration
     * @param medical_ex_expiration
     * @param regs_date
     * @param trainer_regs
     */
    public User(
            long registration,
            String name,
            String email,
            Gender gender,
            Status status,
            Date fitness_ev_expiration,
            Date medical_ex_expiration,
            Date regs_date,
            int trainer_regs
    ) {
        this.name = name;
        this.email = email;
        this.fitness_ev_expiration =  new SimpleDateFormat("dd/MM/yy").format(fitness_ev_expiration);
        this.medical_ex_expiration =  new SimpleDateFormat("dd/MM/yy").format(medical_ex_expiration);
        this.regs_date = new SimpleDateFormat("dd/MM/yy HH:mm").format(regs_date);
        this.status = status;
        this.gender = gender;
        this.registration = registration;
        this.trainer_regs = trainer_regs;
        this.permission = Permission.USER;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFitness_ev_expiration() {
        return fitness_ev_expiration;
    }

    public void setFitness_ev_expiration(String fitness_ev_expiration) {
        this.fitness_ev_expiration = fitness_ev_expiration;
    }

    public String getMedical_ex_expiration() {
        return medical_ex_expiration;
    }

    public void setMedical_ex_expiration(String medical_ex_expiration) {
        this.medical_ex_expiration = medical_ex_expiration;
    }

    public String getRegs_date() {
        return regs_date;
    }

    public void setRegs_date(String regs_date) {
        this.regs_date = regs_date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public long getRegistration() {
        return registration;
    }

    public void setRegistration(long registration) {
        this.registration = registration;
    }

    public long getTrainer_regs() {
        return trainer_regs;
    }

    public void setTrainer_regs(long trainer_regs) {
        this.trainer_regs = trainer_regs;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Permission getPermission() {
        return this.permission;
    }

    public void setPermission(Permission permission) { this.permission = permission; }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return String.format(
                "\nNome:\t%s.\n\tEmail: %s.\n\tMatrícula: %d.\tProfessor: %d\n\tGênero: %s\tSituação: %s\n",
                this.name, this.email, this.registration, this.trainer_regs, this.gender.toString(), this.status.toString()
        );
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", this.name);
        result.put("email", this.email);
        result.put("fitness_ev_expiration", this.fitness_ev_expiration);
        result.put("medical_ex_expiration", this.medical_ex_expiration);
        result.put("regs_date", this.regs_date);
        result.put("status", this.status);
        result.put("gender", this.gender);
        result.put("registration", this.registration);
        result.put("trainer_regs", this.trainer_regs);
        result.put("permission", this.permission);
        return result;
    }
}
