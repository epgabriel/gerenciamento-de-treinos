package ekelf.bodygymapp.activity;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.fragment.register.training.TrainingModuleRegisterFragment;
import ekelf.bodygymapp.fragment.register.training.TrainingRegisterInitialConfigFragment;
import ekelf.bodygymapp.fragment.register.training.TrainingRegistrable;
import ekelf.bodygymapp.model.enums.TrainingModuleType;
import ekelf.bodygymapp.model.enums.TrainingType;
import ekelf.bodygymapp.model.exercise.ExerciseSet;
import ekelf.bodygymapp.model.exercise.WarmingUp;
import ekelf.bodygymapp.model.training.Training;
import ekelf.bodygymapp.model.training.TrainingModule;
import ekelf.bodygymapp.persistence.TrainingDAO;


/**
 * Activity that registers user's training.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-04-10
 */
public class TrainingRegisterActivity extends AppCompatActivity
        implements TrainingRegistrable {

    /** Layout ID for fragment loading. */
    private static final int mainLayout = R.id.TrainingRegisterAct_CL;

    /** Activity progress bar. */
    private ProgressBar progressBar;

    /** Fragment for initial training register configuration */
    private TrainingRegisterInitialConfigFragment trainingRegisterInitialConfigFragment;

    private String userKey, trainingModuleType;

    private Training training;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_register);

        // Activity management view setting.
        progressBar = findViewById(R.id.TrainingRegisterAct_progressBar);
        hideProgressBar();

        userKey = null;
        trainingModuleType = null;
        training = new Training();

        // Loads Initial Config Fragment.
        this.trainingRegisterInitialConfigFragment = new TrainingRegisterInitialConfigFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(mainLayout, trainingRegisterInitialConfigFragment)
                .commit();


    }



    @Override
    public void onUserFind(String key, TrainingModuleType trainingModuleType) {

        for(char c : trainingModuleType.toString().toCharArray()) {
            if (c == 'A') this.training.getTrainingModules().add(new TrainingModule(TrainingType.A));
            else if (c == 'B') this.training.getTrainingModules().add(new TrainingModule(TrainingType.B));
            else if (c == 'C') this.training.getTrainingModules().add(new TrainingModule(TrainingType.C));
            else if (c == 'D') this.training.getTrainingModules().add(new TrainingModule(TrainingType.D));
            else if (c == 'E') this.training.getTrainingModules().add(new TrainingModule(TrainingType.E));
            else if (c == 'F') this.training.getTrainingModules().add(new TrainingModule(TrainingType.F));
            else this.training.getTrainingModules().add(null);
        }

       // Log.i("training", this.training.toString());

        this.userKey = key;
        this.trainingModuleType = trainingModuleType.toString();
        this.training.setTrainingModuleType(trainingModuleType);
    }

    @Override
    public void onTrainingModuleDefined() {

        if(trainingModuleType.isEmpty()) {
            trainingRegistrationFinalStage();
            return;
        }

        Bundle bundle = new Bundle();
        bundle.putChar("Module", trainingModuleType.charAt(0));

        if     (bundle.getChar("Module") == 'A') bundle.putSerializable("TrainingModule", training.getTrainingModules().get(0));
        else if(bundle.getChar("Module") == 'B') bundle.putSerializable("TrainingModule", training.getTrainingModules().get(1));
        else if(bundle.getChar("Module") == 'C') bundle.putSerializable("TrainingModule", training.getTrainingModules().get(2));
        else if(bundle.getChar("Module") == 'D') bundle.putSerializable("TrainingModule", training.getTrainingModules().get(3));
        else if(bundle.getChar("Module") == 'E') bundle.putSerializable("TrainingModule", training.getTrainingModules().get(4));
        else if(bundle.getChar("Module") == 'F') bundle.putSerializable("TrainingModule", training.getTrainingModules().get(5));
        else bundle.putSerializable("TrainingModule", null);


        Fragment fragment = new TrainingModuleRegisterFragment();
        fragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .replace(mainLayout, fragment).commit();
    }

    @Override
    public void onExerciseRegistered(ExerciseSet exerciseSet, int id) {
       // Log.i("exerciseRegistered", oneSet.toString());
        this.training.getTrainingModules()
                .get(id)
                .getExerciseSetList()
                .add(exerciseSet);

        this.onTrainingModuleDefined();
    }

    @Override
    public void onWarmingUpRegistered(WarmingUp warmingUp, int id) {
        this.training.getTrainingModules()
                .get(id)
                .setWarmingUp(warmingUp);
        this.onTrainingModuleDefined();
    }

    @Override
    public void onPosTrainingRegistered(WarmingUp posTraining, int id) {
        this.training.getTrainingModules()
                .get(id)
                .setPosTraining(posTraining);
        this.onTrainingModuleDefined();
    }

    @Override
    public void onDescriptionDefined(String description, int id) {
        this.training
                .getTrainingModules()
                .get(id)
                .setDescription(description);
    }

    @Override
    public void onModuleSet() {
        // Show message that module was concluded
        Toast.makeText(
                getApplicationContext(),
                "Registro do módulo " + trainingModuleType.charAt(0) + " concluído.",
                Toast.LENGTH_SHORT
        ).show();

        // Removes the module already registered from the list
        this.trainingModuleType = (this.trainingModuleType.length() > 1) ?
                this.trainingModuleType.substring(1) : "";

        // Proceed to the new registering stage
        this.onTrainingModuleDefined();
    }

    /** Hides progress bar. */
    private void hideProgressBar() { this.progressBar.setVisibility(View.GONE); }

    /** Shows progress bar. */
    private void showProgressBar() { this.progressBar.setVisibility(View.VISIBLE); }





    /**
     * Final stage of registration. The information being kept will be added to the database.
     */
    public void trainingRegistrationFinalStage(){

        showProgressBar();

        // Shows a message to the user that the training is being registered in the database
        Toast.makeText(
                getApplicationContext(),
                "Cadastrando Treino...",
                Toast.LENGTH_SHORT
        ).show();


        new TrainingDAO().writeObject(
                userKey,
                training.toMap()
        ).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Shows a message to the user that the training was registered in the database successfully
                Toast.makeText(
                        getApplicationContext(),
                        "Treino cadastrado com sucesso!",
                        Toast.LENGTH_SHORT
                ).show();

                // Starts a new fragment register
                hideProgressBar();
                clearObjects();
                getSupportFragmentManager().beginTransaction()
                        .replace(mainLayout, new TrainingRegisterInitialConfigFragment())
                        .commit();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Shows a message to the user that the training was not registered in the database successfully.
                Toast.makeText(
                        getApplicationContext(),
                        "Erro ao cadastrar Treino: " + e.toString(),
                        Toast.LENGTH_SHORT
                ).show();

                clearObjects();
                hideProgressBar();
                getSupportFragmentManager().beginTransaction()
                        .replace(mainLayout, new TrainingRegisterInitialConfigFragment())
                        .commit();
            }
        });

    }

    public void clearObjects() {
        userKey = "";
        trainingModuleType = "";
        training = new Training();
    }

}
