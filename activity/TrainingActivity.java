package ekelf.bodygymapp.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.fragment.dialog.OnWeightUpdate;
import ekelf.bodygymapp.fragment.dialog.DialogWeightChangeFragment;
import ekelf.bodygymapp.management.BGManager;
import ekelf.bodygymapp.model.exercise.ExerciseSet;
import ekelf.bodygymapp.model.exercise.WarmingUp;


/**
 * TrainingModuleType activity.
 * Called by recycler_training activities. Manages one oneSet serie or timing.
 *
 * @author Gabriel Edmilson
 *
 * @version 1.0
 * @since 2019-03-05
 *
 * @version 1.1
 * @since 2019-04-08
 * Add dialog fragment for weight change
 */
public class TrainingActivity extends AppCompatActivity implements OnWeightUpdate {

    /**
     * Training Text Views for Exercise Set: title, rest timing, sets
     * repeats, weight and label of rest.
     */
    private TextView titleText, restTimingText, setsText,
            repeatText, weightText, restLabel;


    /**
     * Buttons: play resting, pause resting, next exercise on set (for N-sets exercises)
     */
    private Button btn_rest_play, btn_rest_pause, btn_next_exercise;


    /** Exercise in training */
    private ExerciseSet exerciseSet;


    /** Beeps for play/pause and repeat finishing */
    private MediaPlayer mp, finish;


    /** Fragment for weight change */
    private DialogWeightChangeFragment dialogWeightChangeFragment;


    private int pos = 0;


    /** Defines what exercise set is being practiced*/
    private int set_id = 0;


    /**
     * Initiates/link variables
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);

        // Makes attributions
        this.titleText      = this.findViewById(R.id.training_title);
        this.restTimingText = this.findViewById(R.id.training_timing);
        this.setsText       = this.findViewById(R.id.training_serie_value);
        this.repeatText     = this.findViewById(R.id.training_rept_value);
        this.weightText     = this.findViewById(R.id.training_weight_value);

        this.weightText.setOnClickListener(openAlertOnClickListener());

        this.btn_rest_play  = this.findViewById(R.id.training_rest_btn_play);
        this.btn_rest_pause = this.findViewById(R.id.training_rest_btn_pause);
        this.btn_next_exercise = this.findViewById(R.id.training_next_btn);
        this.restLabel = this.findViewById(R.id.training_rest_label);

        this.dialogWeightChangeFragment = new DialogWeightChangeFragment();
        this.dialogWeightChangeFragment.setArguments(new Bundle()); // Initiate fragment arguments

        pos = getIntent().getExtras().getInt("id");

        // Retrieves the exercise passed as a parameter between activities
        this.exerciseSet = getIntent().getExtras().getParcelable("exercise");
        Log.i("ExerciseSet", this.exerciseSet.toString());


        // Create medias for beeps
        this.mp = MediaPlayer.create(getApplicationContext(), R.raw.beep);
        this.finish = MediaPlayer.create(getApplicationContext(), R.raw.finish);

        // Hide pause resting time button
        this.btn_rest_pause.setVisibility(View.GONE);
        this.btn_next_exercise.setVisibility(View.GONE);

        // Sets listener for play rest button
        this.btn_rest_play.setOnClickListener(restOnClickListener());


        this.setsText.setText(String.valueOf(exerciseSet.getSeries().getSets()));
        this.repeatText.setText(String.valueOf(exerciseSet.getSeries().getRepeat()));

        // Start exercise set
        if ( pos < 0 ) startWarmingUp();
        else startNewStep();

    }


    public void startWarmingUp() {
        // Sets the current exercise name in the title
        titleText.setText(exerciseSet.getExercise(set_id).getName());


        // Sets the weight of current exercise
        weightText.setText(String.format(
                BGManager.getInstance().getBrazilLocale(),
                "%.1f",
                ((WarmingUp)exerciseSet).getSpeed())
        );


        // Sets "Resting" to rest label
        restLabel.setText(getResources().getString(R.string.training_rest) );

        // Converts the warming-up minutes to seconds and shows in the view
        restTimingText.setText(String.valueOf(exerciseSet.getRest_timing() * 60));
    }

    /**
     * Starts a new step on set counting.
     *
     * First, verify if the current set id is valid (0 >= set id < exercises count on set). If it is,
     * then updates the title, with the exercise's name, and its weight.
     *
     * After that, verify if it is the last exercise of the set (always true if it is a One-set exercise).
     * If it is, then shows resting information. Else, shows info about next exercise.
     */
    public void startNewStep() {

        // If the current set is invalid, finish and show the error
        if ( set_id < 0 || set_id > exerciseSet.getExercises().length) {
            return;
        }


        // Sets the current exercise name in the title
        titleText.setText(exerciseSet.getExercise(set_id).getName());


        // Sets the weight of current exercise
        weightText.setText(String.valueOf(exerciseSet.getExercise(set_id).getWeight()));

        this.restTimingText.setText(String.valueOf(exerciseSet.getRest_timing()));

        // If it is not the last set
        if ( set_id < exerciseSet.getExercises().length - 1 ) {

            // Hides rest timing text and pause/play buttons. Shows next exercise button.
            btn_next_exercise.setVisibility(View.VISIBLE);
            btn_rest_pause.setVisibility(View.GONE);
            btn_rest_play.setVisibility(View.GONE);

            restTimingText.setVisibility(View.INVISIBLE);

            // Resting label becomes the name of the next exercise.
            restLabel.setText(
                    exerciseSet.getExercise(set_id + 1 ).getName()
            );

            btn_next_exercise.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // If everything is ok with the click sound, than plays it
                    if(mp != null) mp.start();

                    set_id++;
                    startNewStep();
                }
            });
        }

        // If it is the last exercise set
        else {

            restTimingText.setVisibility(View.VISIBLE);

            // Hides pause and next button. Show play resting, and rest timing text.
            btn_next_exercise.setVisibility(View.GONE);
            btn_rest_pause.setVisibility(View.GONE);
            btn_rest_play.setVisibility(View.VISIBLE);



            // Sets "Resting" to rest label
            restLabel.setText(getResources().getString(R.string.training_rest) );
        }

    }




    /**
     *  Method for counting down the rest time of the set.
     *
     * @return OnClickListener for resting.
     */
    public View.OnClickListener restOnClickListener() {

        // return a click listener for counting down the resting time.
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // Switches the buttons pause/play
                btn_rest_pause.setVisibility(View.VISIBLE);
                btn_rest_play.setVisibility(View.GONE);

                // If everything is ok with the click sound, than plays it
                if(mp != null) mp.start();

                // Count down timer
                final CountDownTimer count = new CountDownTimer(Integer.parseInt(restTimingText.getText().toString()) * 1000, 1000) {

                    @Override
                    public void onTick(long millisUntilFinished) {

                        String restTiming = ""; // String to be printed
                        long secsUntilFinished = millisUntilFinished / 1000; /*Cast from millis to seconds*/

                        // Puts a zero on the start if there are less than 10s left, and then complete the string with the seconds remaining
                        if (secsUntilFinished < 10) restTiming += "0";
                        restTiming += secsUntilFinished;

                        // Print the remaining rest timing in the display
                        restTimingText.setText(restTiming);
                    }

                    @Override
                    public void onFinish() {

                        // Sets resting time to 0
                        restTimingText.setText("00");

                        // Decrements series value
                        setsText.setText(
                                String.valueOf(
                                        Integer.parseInt(setsText.getText().toString())-1
                                )
                        );


                        // If there is more sets to go, start new net
                        if(Integer.parseInt(setsText.getText().toString()) > 0) {
                            // Show dialog message
                            Toast.makeText(
                                    getApplicationContext(),
                                    getResources().getString(R.string.toast_serie_end),
                                    Toast.LENGTH_LONG
                            ).show();

                            // Reset set id. and start a new step
                            set_id = 0;
                            startNewStep();
                        }

                        // Else, finalize procedures for finishing
                        else {
                            Toast.makeText(
                                    getApplicationContext(),
                                    getResources().getString(R.string.toast_exercise_completed),
                                    Toast.LENGTH_SHORT
                            ).show();

                            // If is everything ok with finish sound, then attributes a listener for it
                            if(finish != null) {
                                finish.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mp) {
                                        finishProcess();
                                    }
                                });
                                finish.start();
                            }

                            // If there is no sound to play, finish immediately
                            else {
                                finishProcess();
                            }
                        }
                    }
                };


                // Configs the pause button on click listener
                btn_rest_pause.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        count.cancel();
                        if(mp != null) mp.start();
                        btn_rest_pause.setVisibility(View.GONE);
                        btn_rest_play.setVisibility(View.VISIBLE);
                    }
                });

                count.start();
            }
        };
    }




    /**
     *  Finishes the activity, running the last updates
     */
    public void finishProcess() {
        //Starts a new intent
        Intent intent = new Intent();

        if (exerciseSet instanceof WarmingUp) {
            intent.putExtra("speed", ((WarmingUp)exerciseSet).getSpeed());
        }

        else{
            //Fulfill the intent with the exercise weights (the only data that can be changed by the user in this activity
            for (int i = 0; i < exerciseSet.getExercises().length; i++){
                intent.putExtra("weight"+String.valueOf(i), exerciseSet.getExercise(i).getWeight());
            }
        }


        //Set the result and finishes this instance activity
        setResult(pos, intent);
        finish();
    }




    /** If the user presses back button, then the exercise wasn't completed.
     *  So, the result code is 0, and the activity is finished */
    @Override
    public void onBackPressed() {
        setResult(0);
        finish();
    }



    /**
     *  Alert fired when start recycler_training button is clicked
     *  @return View.OnClickListener for Alert Firing.
     */
    public View.OnClickListener openAlertOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_next_exercise.setClickable(false);
                btn_rest_play.setClickable(false);
                btn_rest_pause.setClickable(false);

                Bundle bundle = new Bundle();

                if (exerciseSet instanceof WarmingUp) bundle.putFloat("weight", ((WarmingUp) exerciseSet).getSpeed());
                else bundle.putInt("weight", exerciseSet.getExercise(set_id).getWeight());

                dialogWeightChangeFragment.setArguments(bundle);

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.TrainingAct_CL, dialogWeightChangeFragment);
                transaction.commit();
            }
        };
    }



    /**
     * Update oneSet weight. Users can do it at dialog fragment.
     * Here, it is catch.
     *
     * @param newWeight the weight update
     */
    @Override
    public void onWeightUpdate(int newWeight) {
        btn_next_exercise.setClickable(true);
        btn_rest_play.setClickable(true);
        btn_rest_pause.setClickable(true);

        weightText.setText(String.valueOf(newWeight));
        exerciseSet.getExercise(set_id).setWeight(newWeight);
    }

    @Override
    public void onWeightUpdate(float newWeight) {
        btn_next_exercise.setClickable(true);
        btn_rest_play.setClickable(true);
        btn_rest_pause.setClickable(true);

        weightText.setText(
                String.format(
                    BGManager.getInstance().getBrazilLocale(),
                    "%.1f",
                    newWeight
                )
        );

        ((WarmingUp)exerciseSet).setSpeed(newWeight);
    }
}
