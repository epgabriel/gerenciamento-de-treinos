package ekelf.bodygymapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.fragment.register.users.AdminRegisterFragment;
import ekelf.bodygymapp.fragment.register.training.ExerciseRegisterFragment;
import ekelf.bodygymapp.fragment.training.AttendanceFragment;
import ekelf.bodygymapp.fragment.info.ContactFragment;
import ekelf.bodygymapp.fragment.dialog.DialogFragment;
import ekelf.bodygymapp.fragment.register.users.FitnessRegisterFragment;
import ekelf.bodygymapp.fragment.HomeFragment;
import ekelf.bodygymapp.fragment.training.MeasurementsFragment;
import ekelf.bodygymapp.fragment.info.ServicesFragment;
import ekelf.bodygymapp.fragment.register.users.TrainerRegisterFragment;
import ekelf.bodygymapp.fragment.register.users.UserRegisterFragment;
import ekelf.bodygymapp.fragment.training.WorkoutLogFragment;
import ekelf.bodygymapp.management.BGManager;
import ekelf.bodygymapp.model.actors.User;


/**
 * Main activity. Controls all the main properties and fragments of the app.
 *
 * @author Gabriel Edmilson
 * @version 1.0
 * @since 2019-03-05
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    /** Start recycler_training button */
    private FloatingActionButton fab_startTraining;

    /** Workout log fragment */
    private WorkoutLogFragment workoutLogFragment;



    /*  ---------------------------------- Fragments ----------------------------------  */
    /** Home fragment */
    private HomeFragment homeFragment;

    /** Fitness measurements fragment */
    private MeasurementsFragment measurementsFragment;

    /** Attendance fragment */
    private AttendanceFragment attendanceFragment;

    /** Services fragment */
    private ServicesFragment servicesFragment;

    /** Contact fragment */
    private ContactFragment contactFragment;

    /** TrainingModuleType start dialog fragment */
    private DialogFragment dialogFragment;

    private UserRegisterFragment userRegsFragment;

    private TrainerRegisterFragment trainerRegsFragment;

    private FitnessRegisterFragment fitnessRegsFragment;

    private AdminRegisterFragment adminRegisterFragment;

    private ExerciseRegisterFragment exerciseRegisterFragment;

    /*  ---------------------------------- Fragments ----------------------------------  */

    /** Progressbar to show fragment load progress */
    private ProgressBar progressBar;

    /** Auxiliary variable to avoid drawer lag in heavy fragments loading */
    private static int clickedNavItem = 0;

    /** Controls the fragment sequence for back pressed */
    private static List<Integer> backs = new ArrayList<>();

    /** Toolbar reference */
    private Toolbar toolbar;

    /** The drawer menu */
    private DrawerLayout drawer;

    /** The action bar */
    private ActionBarDrawerToggle toggle;

    /** The navigation view */
    private NavigationView navigationView;

    private FirebaseAuth auth;

    private User user;

    /**
     * Initiates/link variables
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.auth = FirebaseAuth.getInstance();

        if(auth.getCurrentUser() == null) signOut();

        this.user = (User) getIntent().getExtras().getSerializable("user");


        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);

        this.drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.toggle = new ActionBarDrawerToggle(
                this, this.drawer, this.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        this.drawer.addDrawerListener(toggle);
        this.toggle.syncState();

        // Configure navigation View
        this.navigationView = (NavigationView) findViewById(R.id.nav_view);
        this.navigationView.setNavigationItemSelectedListener(this);
        switch (this.user.getPermission()) {
            case USER:
                this.navigationView.getMenu().findItem(R.id.menu_user).setVisible(false);
                this.navigationView.getMenu().findItem(R.id.menu_trainer).setVisible(false);
                this.navigationView.getMenu().findItem(R.id.menu_admin).setVisible(false);
                break;

            case TRAINER:
                this.navigationView.getMenu().findItem(R.id.menu_trainer).setVisible(false);
                this.navigationView.getMenu().findItem(R.id.menu_admin).setVisible(false);
        }

        this.navigationView.getMenu().findItem(R.id.menu_info).setVisible(false);

        this.progressBar = findViewById(R.id.main_progressBar);
        this.progressBar.setVisibility(View.GONE);

        this.fab_startTraining = findViewById(R.id.main_fab_start_training);

        /* Instantiates Fragments*/
        this.homeFragment             =  new HomeFragment();
        this.workoutLogFragment       =  new WorkoutLogFragment();
        this.measurementsFragment     =  new MeasurementsFragment();
        this.attendanceFragment       =  new AttendanceFragment();
        this.servicesFragment         =  new ServicesFragment();
        this.contactFragment          =  new ContactFragment();
        this.dialogFragment           =  new DialogFragment();
        this.userRegsFragment         =  new UserRegisterFragment();
        this.fitnessRegsFragment      =  new FitnessRegisterFragment();
        this.trainerRegsFragment      =  new TrainerRegisterFragment();
        this.adminRegisterFragment    =  new AdminRegisterFragment();
        this.exerciseRegisterFragment = new ExerciseRegisterFragment();



        /** Adds drawer listener to drawer - controls fragments loading */
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View view, float v) {
                InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
            }

            @Override
            public void onDrawerOpened(@NonNull View view) {

            }

            @Override
            public void onDrawerStateChanged(int i) {}

            @Override
            public void onDrawerClosed(@NonNull View view) {
                Fragment chosenFrag = null;

                if      (clickedNavItem == R.id.menu_home)         chosenFrag = homeFragment;
                else if (clickedNavItem == R.id.menu_workoutlog)   chosenFrag = workoutLogFragment;
                else if (clickedNavItem == R.id.menu_measurements) chosenFrag = measurementsFragment;
                else if (clickedNavItem == R.id.menu_attendance)   chosenFrag = attendanceFragment;
                else if (clickedNavItem == R.id.menu_services)     chosenFrag = servicesFragment;
                else if (clickedNavItem == R.id.menu_contact)      chosenFrag = contactFragment;

                else if (clickedNavItem == R.id.menu_userRegister)          chosenFrag = userRegsFragment;
                else if (clickedNavItem == R.id.menu_userRegisterFitnessEv) chosenFrag = fitnessRegsFragment;
                else if (clickedNavItem == R.id.menu_userRegisterTraining)  registerTraining();
                else if (clickedNavItem == R.id.menu_trainerRegister)       chosenFrag = trainerRegsFragment;
                else if (clickedNavItem == R.id.menu_adminRegister)         chosenFrag = adminRegisterFragment;


                else if (clickedNavItem == R.id.menu_log_out) signOut();

                if (chosenFrag != null) {
                    if (chosenFrag == homeFragment ) fab_startTraining.show();
                    else fab_startTraining.hide();
                    getSupportFragmentManager().beginTransaction().replace(R.id.main_FL, chosenFrag).commit();
                }



                progressBar.setVisibility(View.GONE);
            }



        });

        //Log.i("list", backs.toString());

        if (clickedNavItem == 0) {
            backs.add(R.id.menu_home);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_FL, this.homeFragment)
                    .commit();
        }



    }

    @Override
    public void onResume() {
        super.onResume();
        backs.add(R.id.menu_home);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_FL, this.homeFragment)
                .commit();
        fab_startTraining.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        fab_startTraining.show();
    }


    /** Alert fired when start recycler_training button is clicked */
    public void openAlert(View view) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.main_FL, this.dialogFragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        else {
            //Log.i("back", this.backs.toString());
            if(this.backs.size() <= 1)
                getSupportFragmentManager().beginTransaction().replace(R.id.main_FL, homeFragment).commit();

            else {

                this.backs.remove(this.backs.size() - 1); //Removes the last fragment reference in the backs list

                Fragment chosenFrag = null; //auxiliary fragment

                // Verifies which fragment object must be handle
                if      (this.backs.get(this.backs.size() - 1) == R.id.menu_home)         chosenFrag = homeFragment;
                else if (this.backs.get(this.backs.size() - 1) == R.id.menu_workoutlog)   chosenFrag = workoutLogFragment;
                else if (this.backs.get(this.backs.size() - 1) == R.id.menu_measurements) chosenFrag = measurementsFragment;
                else if (this.backs.get(this.backs.size() - 1) == R.id.menu_attendance)   chosenFrag = attendanceFragment;
                else if (this.backs.get(this.backs.size() - 1) == R.id.menu_services)     chosenFrag = servicesFragment;
                else if (this.backs.get(this.backs.size() - 1) == R.id.menu_contact)      chosenFrag = contactFragment;
                else if (this.backs.get(this.backs.size() - 1) == R.id.menu_userRegister) chosenFrag = userRegsFragment;
                else if (this.backs.get(this.backs.size() - 1) == R.id.menu_userRegisterFitnessEv)  chosenFrag = fitnessRegsFragment;
                else if (this.backs.get(this.backs.size() - 1) == R.id.menu_adminRegister)          chosenFrag = adminRegisterFragment;
                else if (this.backs.get(this.backs.size() - 1) == R.id.menu_userRegisterTraining)   registerTraining();

                // If was identified a valid fragment, then it replaces the fragment zone
                if (chosenFrag != null) getSupportFragmentManager().beginTransaction().replace(R.id.main_FL, chosenFrag).commit();
                else getSupportFragmentManager().beginTransaction().replace(R.id.main_FL, homeFragment).commit();

            }
        }

        //Log.i("back2", this.backs.toString());
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        //Saves the fragments paths
        this.clickedNavItem = item.getItemId();
        this.backs.add(clickedNavItem);

        // Starts an empty fragment temp. for progress bar showing
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_FL, new Fragment())
                .commit();

        // Set progress bar visibility as visible
        this.progressBar.setVisibility(View.VISIBLE);

        // Closes the drawer
        ((DrawerLayout) findViewById(R.id.drawer_layout))
                .closeDrawer(GravityCompat.START);

        return true;
    }

    /* Exit application */
    public void signOut(){
        // signs user out
        this.auth.signOut();

        // exclude shared login preferences
        SharedPreferences.Editor editor = getSharedPreferences(BGManager.getInstance().getPreferencesFileName(), MODE_PRIVATE).edit();
        editor.remove("login");
        editor.remove("password");
        editor.apply();

        // starts login's activity and finishes main activity
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }

    /* Start training register activity */
    public void registerTraining(){
        startActivity(new Intent(getApplicationContext(), TrainingRegisterActivity.class));
        finish();
    }
}
