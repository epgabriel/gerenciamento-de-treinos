package ekelf.bodygymapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.management.BGManager;
import ekelf.bodygymapp.model.actors.User;
import ekelf.bodygymapp.persistence.UserDAO;

/**
 * Login activity. Controls app access
 *
 * @author Gabriel Edmilson
 * @version 1.0
 * @since 2019-03-05
 */
public class LoginActivity extends AppCompatActivity {

    /** Login edit text */
    private EditText et_login;

    /** Password edit text */
    private EditText et_password;

    /** Access button */
    private Button btn_login;

    /** Progress Bar login */
    private ProgressBar pb_login;


    /**
     * Initiates/link variables
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.et_login    = findViewById(R.id.login_loginEditText);
        this.et_password = findViewById(R.id.login_passwordEditText);
        this.btn_login   = findViewById(R.id.login_button);
        this.pb_login    = findViewById(R.id.login_progressBar);

        this.pb_login.setVisibility(View.GONE);

        // Access user preferences to know if user was logged
        SharedPreferences preferences = getSharedPreferences(BGManager.getInstance().getPreferencesFileName(), MODE_PRIVATE);



        // If user have already being into the app
        if ( preferences.contains("login") && preferences.contains("password") ) {

            // if there is internet connection
            if ( BGManager.getInstance().isConnected(getApplicationContext()) ) {
                final String login    = preferences.getString("login"     , "user_not_found");
                final String password = preferences.getString("password"  , "password_not_found");

                if ( ! login.equals("user_not_found") && ! password.equals("password_not_found") ) {

                    // Fills the user's login fields
                    et_login.setText(login.substring(0, login.indexOf("@")));
                    et_password.setText(password);

                    // Tries to log into the app
                    loginIntoApplication(
                            login, password
                    );
                }

                else
                    /* Configs login button listener to deal with login authentication */
                    this.btn_login.setOnClickListener(loginOnClickListener());
            }

            else
                /* Configs login button listener to deal with login authentication */
                this.btn_login.setOnClickListener(loginOnClickListener());

        }

        else
            /* Configs login button listener to deal with login authentication */
            this.btn_login.setOnClickListener(loginOnClickListener());



    }

    public void loginIntoApplication(final String login, final String password) {
        /* Makes progress bar visible */
        pb_login.setVisibility(View.VISIBLE);

        /* Verifies if the connection is set */
        if ( BGManager.getInstance().isConnected(getApplicationContext()) ) {

            /* Tries to sign in with email and password, and tries to authenticate it */
            FirebaseAuth.getInstance().signInWithEmailAndPassword(
                    login,
                    password
            ).addOnCompleteListener(new OnCompleteListener<AuthResult>() {

                /* Verifies if the login attempt occurred successfully */
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    /* if occurred successfully, then start the main activity */
                    if (task.isSuccessful()) {
                        //Log.i("login", "Login successful");
                        UserDAO userDAO = new UserDAO();
                        userDAO.getReference().child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        User user = dataSnapshot.getValue(User.class);
                                        //Log.i("userLoad", user.getName());
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        intent.putExtra("user", user);
                                        startActivity(intent);

                                        SharedPreferences preferences = getSharedPreferences(BGManager.getInstance().getPreferencesFileName(), MODE_PRIVATE);
                                        preferences.edit().putString("login", login).apply();
                                        preferences.edit().putString("password", password).apply();

                                        finish();
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        clear();
                                        pb_login.setVisibility(View.GONE);
                                    }
                                });
                    }

                    /* Else, shows a message */
                    else {
                        //Log.i("login", "Login failed");
                        pb_login.setVisibility(View.GONE);
                        Toast.makeText(
                                getApplicationContext(),
                                "Erro, verifique seus dados",
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                }
            });
        }

        else {
            //Log.i("login", "Login failed, connection not found");
            pb_login.setVisibility(View.GONE);
            Toast.makeText(
                    getApplicationContext(),
                    "Verifique sua conexão com a internet",
                    Toast.LENGTH_SHORT
            ).show();
        }
    }

    public View.OnClickListener loginOnClickListener() {
        return new View.OnClickListener() {

            /* Method On Click - Login */
            @Override
            public void onClick(View v) {

                // Do nothing if the form isn't valid
                if(!validForm()) return;

                // Else, tries to enter into the application
                loginIntoApplication(
                        et_login.getText().toString() + getResources().getString(R.string.app_email_format),
                        et_password.getText().toString()
                );
            }
        };
    }



    public boolean validForm(){
        boolean valid = true;

        if (this.et_login.getText().toString().isEmpty()){
            this.et_login.setError("Campo obrigatório");
            valid = false;
        } else this.et_login.setError(null);

        if (this.et_password.getText().toString().isEmpty()) {
            this.et_password.setError("Campo obrigatório");
            valid = false;
        } else this.et_password.setError(null);

        return valid;
    }

    public void clear() {
        this.et_login.setText(null);
        this.et_login.setError(null);
        this.et_password.setText(null);
        this.et_password.setError(null);
    }
}
