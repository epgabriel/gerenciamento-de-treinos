package ekelf.bodygymapp.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.listener.RecyclerItemClickListener;
import ekelf.bodygymapp.adapter.AdapterTrainingSheet;
import ekelf.bodygymapp.model.exercise.ExerciseSet;
import ekelf.bodygymapp.model.training.TrainingModule;
import ekelf.bodygymapp.model.enums.TrainingType;
import ekelf.bodygymapp.model.exercise.WarmingUp;


/** TrainingModuleType sheet activity.
 * It shows the programming recycler_training initiated.
 *
 * @author Gabriel Edmilson
 * @version 1.0
 * @since 22019-03-05
 */
public class TrainingSheetActivity extends AppCompatActivity {

    /** Exercises recycler view */
    private RecyclerView recyclerView;

    /** Exercises list */
    private TrainingModule trainingModule;

    /** Title text view */
    private TextView tv_title;

    /** Warming-up button */
    private Button btn_warming_up, btn_pos_training;

    private List<Map<String, Object>> trainingMapDone;

    private List<String> exerciseSetListBackup;

    /**
     * Initiates/link variables
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_sheet);


        this.recyclerView = findViewById(R.id.training_sheet_training_LL_RV);
        this.tv_title     = findViewById(R.id.training_sheet_title);
        this.btn_warming_up   = findViewById(R.id.training_sheet_warming_up_LL_exercise);
        this.btn_pos_training = findViewById(R.id.training_sheet_pos_training_LL_exercise);

        this.trainingModule = new TrainingModule(
                TrainingType.A,
                "Superiores",
                new WarmingUp("Esteira", 0, (float) 8.0),
                new ArrayList<ExerciseSet>(),
                new WarmingUp("Esteira", 0, (float) 5.4)
        );


        createExercises();

        this.trainingMapDone = new ArrayList<>();
        this.exerciseSetListBackup = new ArrayList<>();

        //Warming up
        this.trainingMapDone.add(new HashMap<String, Object>());

        this.exerciseSetListBackup.add(trainingModule.getWarmingUp().exerciseName());
        for (ExerciseSet e : this.trainingModule.getExerciseSetList()) {
            this.exerciseSetListBackup.add(e.exerciseName());
            this.trainingMapDone.add(new HashMap<String, Object>());
        }
        this.exerciseSetListBackup.add(trainingModule.getPosTraining().exerciseName());

        //Pos-training, if the training has one
        this.trainingMapDone.add(new HashMap<String, Object>());


        this.tv_title.setText("TREINO A");
        this.btn_warming_up.setText("Esteira - 10 minutos");
        this.btn_pos_training.setText("Esteira - 10 minutos");

        this.recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        this.recyclerView.setHasFixedSize(false);
        this.recyclerView.setAdapter(new AdapterTrainingSheet(this.trainingModule.getExerciseSetList()));

        //evento de click
        this.recyclerView.addOnItemTouchListener(recyclerItemClickListener());


        // click listener
        this.btn_warming_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TrainingActivity.class);

                intent.putExtra("exercise", trainingModule.getWarmingUp());
                intent.putExtra("id", -1);
                startActivityForResult(intent, 1);
            }
        });

        this.btn_pos_training.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TrainingActivity.class);
                intent.putExtra("exercise", trainingModule.getPosTraining());
                intent.putExtra("id", -1);
                startActivityForResult(intent, 3);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("ResultCode", String.valueOf(resultCode));

        if(requestCode == 2 && resultCode >= 0) {

            // Modify the weight (if it has been changed in training
            for (int i = 0; i < trainingModule.getExerciseSetList().get(resultCode).getExercises().length; i++){
                if (data != null && data.getExtras().containsKey("weight" + String.valueOf(i))) {
                    this.trainingModule.getExerciseSetList().get(resultCode).getExercise(i).setWeight(
                            data.getExtras().getInt("weight" + String.valueOf(i))
                    );
                }
            }

            int originalId = exerciseSetListBackup.indexOf(
                    trainingModule.getExerciseSetList().get(resultCode).exerciseName()
            );

            trainingMapDone.get(originalId).put(
                    "exercise" + String.valueOf(originalId),
                    trainingModule.getExerciseSetList().get(resultCode).toMap()
            );

            // Removes the exercise from the list
            this.trainingModule.getExerciseSetList().remove(resultCode);

            // Notify the adapter that this exercise had been removed
            this.recyclerView.getAdapter().notifyItemRemoved(resultCode);
        }

        else if (requestCode == 1 && resultCode == -1) {

            if (data != null && data.getExtras().containsKey("speed")) {
                this.trainingModule.getWarmingUp().setSpeed(
                        data.getExtras().getFloat("speed")
                );
            }

            // Puts warming up in the map
            trainingMapDone.get(0).put(
                    "warmingup",
                    trainingModule.getWarmingUp().toMap()
            );

            this.btn_warming_up.setVisibility(View.GONE);

        }

        else if ( requestCode == 3 && resultCode == -1 && trainingModule.getPosTraining() != null) {
            if (data.getExtras() != null && data.getExtras().containsKey("speed")) {
                this.trainingModule.getPosTraining().setSpeed(
                        data.getExtras().getFloat("speed")
                );
            }

            trainingMapDone.get(exerciseSetListBackup.size() - 1).put(
                    "postraining",
                    trainingModule.getPosTraining().toMap()
            );

            this.btn_pos_training.setVisibility(View.GONE);
        }

        Log.i("maps", this.trainingMapDone.toString());
        if(isTrainingDone()){
            Log.i("trainingDone", "its done!");
        }

    }

    public RecyclerItemClickListener recyclerItemClickListener() {
        return new RecyclerItemClickListener(
                getApplicationContext(),
                this.recyclerView,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        if(trainingModule.getExerciseSetList().get(position) != null) {
                            Intent intent = new Intent(getApplicationContext(), TrainingActivity.class);

                            intent.putExtra("exercise", trainingModule.getExerciseSetList().get(position));
                            intent.putExtra("id", position);
                            startActivityForResult(intent, 2);
                        } else{
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Item pressionado: " + position,
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        ExerciseSet exerciseSet = trainingModule.getExerciseSetList().get(position);
                        Toast.makeText(
                                getApplicationContext(),
                                "Click Longo: " + exerciseSet.getExercise().getName(),
                                Toast.LENGTH_SHORT
                        ).show();
                    }

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    }
                }
        );
    }

    public boolean isTrainingDone() {
        boolean done = false;
        for(Map<String, Object> m : trainingMapDone) done |= m.isEmpty();
        return !done;
    }

    public void createExercises() {
        ExerciseSet e1 = new ExerciseSet();
        e1.getExercise().setName("Banco Soleo");
        e1.getSeries().setSets(1);
        e1.getSeries().setRepeat(25);
        e1.getExercise().setWeight(0);
        e1.setRest_timing(1);

        ExerciseSet e2 = new ExerciseSet();
        e2.getExercise().setName("Panturrilha Barra Guiada");
        e2.getSeries().setSets(1);
        e2.getSeries().setRepeat(25);
        e2.getExercise().setWeight(0);
        e2.setRest_timing(1);

        ExerciseSet e3 = new ExerciseSet();
        e3.getExercise().setName("Dors Flexão Leg45°");
        e3.getSeries().setSets(1);
        e3.getSeries().setRepeat(25);
        e3.getExercise().setWeight(0);
        e3.setRest_timing(1);

        ExerciseSet e4 = new ExerciseSet();
        e4.getExercise().setName("Panturrilha Abduzido Hack");
        e4.getSeries().setSets(1);
        e4.getSeries().setRepeat(25);
        e4.getExercise().setWeight(0);
        e4.setRest_timing(1);

        ExerciseSet e5 = new ExerciseSet(2);
        e5.getSeries().setSets(1);
        e5.getSeries().setRepeat(25);
        e5.setRest_timing(1);
        e5.getExercise(0).setName("Panturrilha Abduzido Hack");
        e5.getExercise(0).setWeight(20);
        e5.getExercise(1).setName("Prancha Frontal");
        e5.getExercise(1).setWeight(10);

        ExerciseSet e6 = new ExerciseSet();
        e6.getExercise().setName("Prancha Frontal");
        e6.getSeries().setSets(1);
        e6.getSeries().setRepeat(25);
        e6.getExercise().setWeight(0);
        e6.setRest_timing(1);

        this.trainingModule.getExerciseSetList().add(e1);
        this.trainingModule.getExerciseSetList().add(e2);
        this.trainingModule.getExerciseSetList().add(e3);
        this.trainingModule.getExerciseSetList().add(e4);
        this.trainingModule.getExerciseSetList().add(e5);
        this.trainingModule.getExerciseSetList().add(e6);
    }
}
