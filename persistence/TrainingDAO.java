package ekelf.bodygymapp.persistence;


import com.google.firebase.database.FirebaseDatabase;


/**
 * TrainingModuleType Data Access Object Class.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-04
 */
public class TrainingDAO extends PersistenceDAO {

    /**
     * Constructs a new data access object for training root.
     */
    public TrainingDAO() {
        super(
                FirebaseDatabase.getInstance().getReference().child("training"),
                "/training"
        );
    }

}
