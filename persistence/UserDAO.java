package ekelf.bodygymapp.persistence;



import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.Map;


/**
 * Persistence user class.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-15
 */
public class UserDAO extends PersistenceDAO{

    /**
     * Constructs a new data access object for user root.
     */
    public UserDAO(){
        super(
                FirebaseDatabase.getInstance().getReference().child("users"),
                "/users/"
        );
    }


    public Query queryRegistration() {
        return super.getReference().orderByChild("registration");
    }

    @Override
    public Task<Void> writeObject(String userID, Map<String, Object> objectValues) {
        return super.getReference().child(userID).setValue(objectValues);
    }

    @Override
    public Task<Void> updateObject(String userID, Map<String, Object> objectValues) {
        return null;
    }

    @Override
    public Task<Void> removeObject(String path) {
        return null;
    }
}
