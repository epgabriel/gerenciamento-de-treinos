package ekelf.bodygymapp.persistence;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

/**
 * Fitness Evaluation Data Access Object Class.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-27-03
 */
public class FitnessDAO extends PersistenceDAO{

    /**
     * Constructs a new data access object for fitness evaluation root.
     */
    public FitnessDAO() {
        super(
                FirebaseDatabase.getInstance().getReference().child("fitness_ev"),
                "/fitness_ev/"
        );
    }


    /** @return query for children ordered by version */
    public Query childOrderByVersion() { return super.getReference().orderByChild("version"); }

}
