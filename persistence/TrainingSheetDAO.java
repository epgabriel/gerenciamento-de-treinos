package ekelf.bodygymapp.persistence;

import com.google.firebase.database.FirebaseDatabase;

public class TrainingSheetDAO extends PersistenceDAO{


    public TrainingSheetDAO() {
        super(
                FirebaseDatabase.getInstance().getReference().child("training_sheet"),
                "/training_sheet/"
        );
    }


}
