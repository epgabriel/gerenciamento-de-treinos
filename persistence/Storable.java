package ekelf.bodygymapp.persistence;

import com.google.android.gms.tasks.Task;

import java.util.Map;

/**
 * Interface to manage persistence classes.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-04-09
 */
public interface Storable {


    /**
     * Method to write objects on the Data Base
     *
     * @param userID user identification on database
     * @param objectValues objects with keys
     * @return
     */
    Task<Void> writeObject(
            String userID,
            Map<String, Object> objectValues
    );



    /**
     * Method to update objects on the Data Base
     *
     * @param userID user identification on database
     * @param objectValues objects with keys
     * @return
     */
    Task<Void> updateObject(
            String userID,
            Map<String, Object> objectValues
    );



    /**
     * Method to update objects on the Data Base
     *
     * @param path to the objects that will be removed.
     * @return
     */
    Task<Void> removeObject(
            String path
    );

}
