package ekelf.bodygymapp.persistence;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;


/**
 * Abstract class for instances of Data Access Objects.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-04-09
 */
public abstract class PersistenceDAO implements Storable{

    /** Database reference to a root node in database. */
    private DatabaseReference reference;

    /** Path to the node reference. */
    private String path;


    /**
     * Default constructor for persistence classes.
     * @param reference
     * @param path
     */
    public PersistenceDAO(DatabaseReference reference, String path) {
        this.reference = reference;
        this.path = path;
    }


    /** @return a reference to the specific database node being used. */
    public DatabaseReference getReference() { return this.reference; }


    /** @return node reference path. */
    public String getPath() { return this.path; }


    @Override
    public Task<Void> writeObject(String userID, Map<String, Object> objectValues) {
        String key = this.reference.child(userID).push().getKey();
        Map <String, Object> childUpdate = new HashMap<>();
        childUpdate.put(userID + "/" + key, objectValues);
        return this.reference.updateChildren(childUpdate);
    }

    @Override
    public Task<Void> updateObject(String userID, Map<String, Object> objectValues) {
        return null;
    }

    @Override
    public Task<Void> removeObject(String path) {
        return null;
    }

}
