package ekelf.bodygymapp.fragment.dialog;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import ekelf.bodygymapp.R;
import ekelf.bodygymapp.activity.TrainingSheetActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogFragment extends Fragment {

    private TextView alert_title, alert_body;

    /** Buttons for negative and positive response */
    private Button negative_response, positive_response;



    public DialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View dialogView = inflater.inflate(R.layout.alert_dialog, container, false);

        alert_title = dialogView.findViewById(R.id.dialog_title);
        alert_title.setText(getActivity().getResources().getString(R.string.alert_training_start_title));

        alert_body = dialogView.findViewById(R.id.dialog_body);
        alert_body.setText(" ");

        this.negative_response = dialogView.findViewById(R.id.dialog_btn_negative);
        this.negative_response.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager()
                        .beginTransaction()
                        .remove(getFragmentManager().getFragments().get(
                                getFragmentManager().getFragments().size()-1
                        ))
                        .commit();
            }
        });


        this.positive_response = dialogView.findViewById(R.id.dialog_btn_positive);
        this.positive_response.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TrainingSheetActivity.class);
                startActivity(intent);
                getActivity().getSupportFragmentManager().beginTransaction().remove(DialogFragment.this).commit();

            }
        });

        return dialogView;
    }

}
