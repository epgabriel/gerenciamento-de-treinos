package ekelf.bodygymapp.fragment.dialog;

/**
 * Interface for weight changing.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-04-09
 */
public interface OnWeightUpdate {
    void onWeightUpdate(int newWeight);
    void onWeightUpdate(float newWeight);
}
