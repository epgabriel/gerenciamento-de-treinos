package ekelf.bodygymapp.fragment.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import ekelf.bodygymapp.R;
import ekelf.bodygymapp.management.BGManager;

/**
 * A simple {@link Fragment} subclass.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-06-04
 */
public class DialogWeightChangeFragment extends Fragment {

    private OnWeightUpdate dataPasser;

    private TextView tv_weight;

    private Bundle bundle;

    public DialogWeightChangeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (OnWeightUpdate) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Button btn_up, btn_down, btn_save;

        View dialogView = inflater.inflate(R.layout.alert_dialog_weight_change, container, false);

        btn_up     = dialogView.findViewById(R.id.dialogWeightChange_btn_up);
        btn_up.setOnClickListener(increaseWeightListener());

        btn_down   = dialogView.findViewById(R.id.dialogWeightChange_btn_down);
        btn_down.setOnClickListener(decreaseWeightListener());

        //this.btn_cancel = dialogView.findViewById(R.id.dialogWeightChange_btn_cancel);
        btn_save   = dialogView.findViewById(R.id.dialogWeightChange_btn_save);


        this.tv_weight = dialogView.findViewById(R.id.dialogWeightChange_tv_weight);
        this.bundle = getArguments();

        if ( bundle != null && bundle.containsKey("weight") ) {
            if ( bundle.getInt("weight", -1) >= 0) {
                this.tv_weight.setText(
                        String.valueOf(bundle.getInt("weight"))
                );

                btn_save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dataPasser.onWeightUpdate(Integer.parseInt(tv_weight.getText().toString()));
                        closeDialog();
                    }
                });

                // Sets "cancel" on click listener
                dialogView.findViewById(R.id.dialogWeightChange_btn_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dataPasser.onWeightUpdate(bundle.getInt("weight"));
                        closeDialog();
                    }
                });
            }
            else {
                this.tv_weight.setText(
                        String.format(
                                BGManager.getInstance().getBrazilLocale(),
                                "%.1f",
                                bundle.getFloat("weight")
                        )
                );

                btn_save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dataPasser.onWeightUpdate(Float.parseFloat(tv_weight.getText().toString().replace(',', '.')));
                        closeDialog();
                    }
                });

                // Sets "cancel" on click listener
                dialogView.findViewById(R.id.dialogWeightChange_btn_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dataPasser.onWeightUpdate(bundle.getFloat("weight"));
                        closeDialog();
                    }
                });

            }

        }



        return dialogView;
    }



    /**
     * Closes dialog
     */
    public void closeDialog() {
        getFragmentManager()
                .beginTransaction()
                .remove(getFragmentManager().getFragments().get(
                        getFragmentManager().getFragments().size()-1
                ))
                .commit();
    }



    /**
     * @return OnClickListener for weight increment
     */
    public View.OnClickListener increaseWeightListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( bundle != null && bundle.containsKey("weight") ) {
                    if (bundle.getInt("weight", -1) >= 0) {

                        // Shows to the user the exercise's weight plus one
                        tv_weight.setText(String.valueOf(
                                (Integer.parseInt(tv_weight.getText().toString()) + 1)
                        ));
                    } else {
                        // Shows to the user the exercise's weight plus one
                        tv_weight.setText(String.valueOf(
                                String.format(
                                        BGManager.getInstance().getBrazilLocale(),
                                        "%.1f",
                                        (Float.parseFloat(tv_weight.getText().toString().replace(',', '.')) + 0.5f)
                                )
                        ));
                    }
                }
            }
        };
    }



    /**
     * @return OnClickListener for weight decrement
     */
    public View.OnClickListener decreaseWeightListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if ( bundle != null && bundle.containsKey("weight") ) {
                if (bundle.getInt("weight", -1) >= 0) {

                    // Shows to the user the exercise's weight plus one
                    tv_weight.setText(String.valueOf(
                            (Integer.parseInt(tv_weight.getText().toString()) > 0) ?
                                    (Integer.parseInt(tv_weight.getText().toString()) - 1) : 0
                    ));
                } else {
                    // Shows to the user the exercise's weight plus one
                    tv_weight.setText(
                            String.format(
                                BGManager.getInstance().getBrazilLocale(),
                                "%.1f",
                                (Float.parseFloat(tv_weight.getText().toString().replace(',', '.')) > 0.0f) ?
                                (Float.parseFloat(tv_weight.getText().toString().replace(',', '.')) - 0.1f) : 0.0f
                    ));
                }
            }
            }
        };
    }



}
