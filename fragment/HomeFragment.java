package ekelf.bodygymapp.fragment;


import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.fragment.register.Registrable;
import ekelf.bodygymapp.management.BGManager;
import ekelf.bodygymapp.model.FitnessEvaluation;
import ekelf.bodygymapp.model.enums.TrainingType;
import ekelf.bodygymapp.model.exercise.ExerciseSet;
import ekelf.bodygymapp.model.exercise.WarmingUp;
import ekelf.bodygymapp.model.training.Training;
import ekelf.bodygymapp.persistence.FitnessDAO;
import ekelf.bodygymapp.persistence.TrainingDAO;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements Registrable {

    /** TrainingModuleType title text view */
    private TextView tv_trainingTitle;

    /** TrainingModuleType body text view */
    private TextView tv_trainingBody;

    /** TrainingModuleType Box Button */
    private Button btn_trainingDay;

    /** Fitness current weight text view */
    private TextView tv_currentWeight;

    /** Fitness intent weight text view */
    private TextView tv_intentWeight;

    /** Fitness goal weight text view */
    private TextView tv_goalWeight;

    /** Fitness BMI text view */
    private TextView tv_BMI;

    /** Fitness fat rate text view */
    private TextView tv_fatRate;

    /** Fitness ideal weight text view */
    private TextView tv_idealWeight;

    /** Fitness evaluation progress bar */
    private ProgressBar pb_training, pb_fitness;

    /** Fitness Evaluation Constraint Layout */
    private ConstraintLayout cl_fitness;



    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View homeView = inflater.inflate(R.layout.fragment_home, container, false);

        this.tv_trainingTitle =  homeView.findViewById(R.id.main_trainingbox_title);
        this.tv_trainingBody  =  homeView.findViewById(R.id.main_trainingbox_body);
        this.tv_currentWeight =  homeView.findViewById(R.id.main_myWeight_box_tv_currentWeight_value);
        this.tv_intentWeight  =  homeView.findViewById(R.id.main_myWeight_box_tv_intent_value);
        this.tv_goalWeight    =  homeView.findViewById(R.id.main_myWeight_box_tv_goal_value);
        this.tv_BMI           =  homeView.findViewById(R.id.main_myWeight_box_tv_BMI_value);
        this.tv_fatRate       =  homeView.findViewById(R.id.main_myWeight_box_tv_fatRate_value);
        this.tv_idealWeight   =  homeView.findViewById(R.id.main_myWeight_box_tv_idealWeight_value);


        // Configs training progress bar
        this.pb_training = homeView.findViewById(R.id.main_trainingPB);
        this.pb_training.setVisibility(View.VISIBLE);


        // Configs training day button
        this.btn_trainingDay  =  homeView.findViewById(R.id.main_btn_dayTraining);


        // Configs fitness ev. progress bar
        this.pb_fitness       =  homeView.findViewById(R.id.main_myWeightPB);
        this.pb_fitness.setVisibility(View.VISIBLE);


        // Configs fitness ev. constraint layout
        this.cl_fitness = homeView.findViewById(R.id.main_constraintLayout_meuPeso);
        this.cl_fitness.setVisibility(View.GONE);

        loadTrainingInfo();
        loadFitnessInfo();

        return homeView;
    }



    /** Loads recycler_training information as day's recycler_training and data related to it */
    public void loadTrainingInfo() {

        // Loads today abbreviation (e. g. Sunday -> SUN ).
        this.btn_trainingDay.setText(
                getContext().getResources().getString(
                        BGManager.getInstance().todayAbrvID()
                )
        );

        // Loads training info - if exists.
        new TrainingDAO()
                .getReference()
                .orderByKey()
                .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addListenerForSingleValueEvent(listenerLoadTrainingInfo());
    }



    /** Loads fitness information of the user */
    public void loadFitnessInfo() {

        // Call the server
        new FitnessDAO()
                .getReference()
                .orderByKey()
                .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addListenerForSingleValueEvent(listenerLoadFitnessInfo());

    }




    /** Shows the information retrieved from the database. */
    public void showFitnessEvaluationInfo(FitnessEvaluation fitnessEvaluation) {
        this.tv_currentWeight.setText(fitnessEvaluation.getWeightFormatted());
        this.tv_intentWeight.setText(fitnessEvaluation.getGoalFormatted());
        this.tv_goalWeight.setText(String.valueOf(fitnessEvaluation.getPoundsToGoalFormatted()));
        this.tv_BMI.setText(fitnessEvaluation.getBMIFormatted());
        this.tv_fatRate.setText(fitnessEvaluation.getFatRateFormatted());
        this.tv_idealWeight.setText(fitnessEvaluation.getIdealWeightFormatted());


        this.pb_fitness.setVisibility(View.GONE);    // hide progress bar
        this.cl_fitness.setVisibility(View.VISIBLE); // shows fitness ev layout
    }

    /**
     * Attempts to retrieve user's most recent training module.
     * @return listener for fitness evaluation child.
     */
    private ValueEventListener listenerLoadTrainingInfo() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {



                // If user has training registered
                if(dataSnapshot.hasChildren()) {
                    dataSnapshot.getChildren()
                            .iterator()
                            .next()
                            .getRef()
                            .orderByChild("version")
                            .limitToLast(1)
                            .addListenerForSingleValueEvent(listenerTrainingExists());
                }

                // User has no training registered.
                else {
                    finalizeWithFailure("Nenhum treino cadastrado.");
                    pb_training.setVisibility(View.GONE);
                    showTrainingMsg(
                            "Usuário não possui treinos cadastrados.",
                            "Procure seu professor para inicar seus treinos!"
                    );
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finalizeWithFailure(databaseError.getMessage());
                pb_training.setVisibility(View.GONE);
                showTrainingMsg(
                        "Usuário não possui treinos cadastrados.",
                        "Procure seu professor para inicar seus treinos!"
                );
            }
        };
    }


    /**
     * User exists in the fitness evaluation area, so listen to the last fitness evaluation.
     * @return Listener for the last fitness evaluation made for the user.
     */
    private ValueEventListener listenerTrainingExists() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Training training;
                training = dataSnapshot.getChildren()
                        .iterator()
                        .next()
                        .getValue(Training.class);

                if (training == null) {
                    pb_training.setVisibility(View.GONE);
                    showTrainingMsg(
                            "Usuário não possui treinos cadastrados.",
                            "Procure seu professor para inicar seus treinos!"
                    );

                    return;
                }

                training.initiateTrainingModules(); // starts training modules according to the type (AB, ABC, etc)

                for (DataSnapshot ds : dataSnapshot.getChildren().iterator().next().getChildren()) {
                    Log.i("ds1Values", ds.toString());
                    //Log.i("ds1ValuesLength", ds1.getValue().toString());

                    String key = ds.getKey(); // Keeps the key

                    // If it is a module, then its needed to slice its attributes
                    if (key.contains("module") && key.length() <= 8){

                        int id = Character.getNumericValue( key.charAt(key.length() - 1) );

                        // Transform the Training Module in Maps

                        GenericTypeIndicator<Map<String, Object>> t = new GenericTypeIndicator<Map<String, Object>>() {};
                        Map<String, Object> tm_map = ds.getValue(t);

                        // Configs warming up
                        if (tm_map != null && tm_map.containsKey("warming_up")) {
                            WarmingUp warmingUp = WarmingUp.buildFromMap( (HashMap<String, Object>) tm_map.get("warming_up") );
                            training.getTrainingModules().get(id).setWarmingUp(warmingUp);
                        }

                        // Configs pos training
                        if (tm_map != null && tm_map.containsKey("pos_training") ) {
                            WarmingUp posTraining = WarmingUp.buildFromMap( (HashMap<String, Object>) tm_map.get("pos_training") );
                            training.getTrainingModules().get(id).setPosTraining(posTraining);
                        }

                        // Configs pos training
                        if (tm_map != null && tm_map.containsKey("description") ) {
                            training.getTrainingModules().get(id).setDescription(String.valueOf(tm_map.get("description")));
                        }

                        // Configs training type
                        if (tm_map != null && tm_map.containsKey("type") ) {
                            switch ( String.valueOf(tm_map.get("type")) ) {
                                case "A" : training.getTrainingModules().get(id).setType(TrainingType.A); break;
                                case "B" : training.getTrainingModules().get(id).setType(TrainingType.B); break;
                                case "C" : training.getTrainingModules().get(id).setType(TrainingType.C); break;
                                case "D" : training.getTrainingModules().get(id).setType(TrainingType.D); break;
                                case "E" : training.getTrainingModules().get(id).setType(TrainingType.E); break;
                                case "F" : training.getTrainingModules().get(id).setType(TrainingType.F); break;
                            }
                        }

                        // Configs Exercise Set
                        if ( tm_map != null && tm_map.containsKey("exercise_list")) {
                            List exerciseList = (ArrayList) tm_map.get("exercise_list");
                            if (exerciseList != null) {
                                for (Object obj : exerciseList) {
                                    training.getTrainingModules().get(id).getExerciseSetList().add(
                                            ExerciseSet.buildFromMap((HashMap)obj)
                                    );
                                }
                            }
                        }


                        for (String s : tm_map.keySet()) {

                            // Configs warming up
                            if ( s.equals("warming_up") ) {
                                WarmingUp warmingUp = WarmingUp.buildFromMap( (HashMap<String, Object>) tm_map.get(s) );
                                training.getTrainingModules().get(id).setWarmingUp(warmingUp);
                            }

                            // Configs pos training
                            else if ( s.equals("pos_training") ) {
                                WarmingUp posTraining = WarmingUp.buildFromMap( (HashMap<String, Object>) tm_map.get(s) );
                                training.getTrainingModules().get(id).setPosTraining(posTraining);
                            }
                        }

                    }
                }



                Log.i("training", training.toMap().toString());


                int id = training.getTrainingDaysCount() % training.getTrainingModuleType().toString().length();

                String title = "";
                title += "Treino de Hoje é ";
                title += training.getTrainingModules().get(id).getType().toString();
                tv_trainingTitle.setText( title );

                String body = "Modalidade: " + training.getTrainingModules().get(id).getDescription() +"\n";
                body += "- Treino " + training.getTrainingDaysCount() + "/" + training.getTotalDaysToTrainingComplete() + "\n";
                tv_trainingBody.setText( body );

                pb_training.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finalizeWithFailure(databaseError.getMessage());
                pb_training.setVisibility(View.GONE);
                showTrainingMsg(
                        "Usuário não possui treinos cadastrados.",
                        "Procure seu professor para inicar seus treinos!"
                );
            }
        };
    }

    /**
     * User exists in the fitness evaluation area, so listen to the last fitness evaluation.
     * @return Listener for the last fitness evaluation made for the user.
     */
    private ValueEventListener listenerChildTrainingExists() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.i("trainingModule", dataSnapshot.toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finalizeWithFailure(databaseError.getMessage());
                pb_fitness.setVisibility(View.GONE);
            }
        };
    }


    /**
     * Attempts to retrieve user's most recent fitness evaluation.
     * @return listener for fitness evaluation child.
     */
    private ValueEventListener listenerLoadFitnessInfo() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // If user has fitness evaluation(s) registered
                if(dataSnapshot.hasChildren())
                    dataSnapshot.getChildren()
                            .iterator()
                            .next()
                            .getRef()
                            .orderByChild("version")
                            .limitToLast(1)
                            .addListenerForSingleValueEvent(listenerChildExists());

                // User has no fitness evaluations registered, then loads empty information '-'.
                else {
                    finalizeWithFailure("Nenhuma avaliação cadastrada.");
                    pb_fitness.setVisibility(View.GONE);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finalizeWithFailure(databaseError.getMessage());
                pb_fitness.setVisibility(View.GONE);
            }
        };
    }



    /**
     * User exists in the fitness evaluation area, so listen to the last fitness evaluation.
     * @return Listener for the last fitness evaluation made for the user.
     */
    private ValueEventListener listenerChildExists() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showFitnessEvaluationInfo(
                        dataSnapshot.getChildren()
                                .iterator()
                                .next()
                                .getValue(FitnessEvaluation.class)
                );
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finalizeWithFailure(databaseError.getMessage());
                pb_fitness.setVisibility(View.GONE);
            }
        };
    }



    /** Shows training message */
    public void showTrainingMsg(String title, String body) {
        this.tv_trainingTitle.setText(title);
        this.tv_trainingBody.setText(body);
        this.pb_training.setVisibility(View.GONE);
    }

    @Override
    public void finalizeNotFilled(){}

    @Override
    public void finalizeWithSuccess(){}

    @Override
    public void finalizeWithFailure(String e) {
        Toast.makeText(
                getContext(),
                e,
                Toast.LENGTH_SHORT
        ).show();
    }

    @Override
    public boolean validForm() {
        return false;
    }
}
