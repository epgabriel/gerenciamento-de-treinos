package ekelf.bodygymapp.fragment.training;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.adapter.AdapterAttendance;
import ekelf.bodygymapp.fragment.HomeFragment;
import ekelf.bodygymapp.model.Attendance;
import ekelf.bodygymapp.model.enums.TrainingType;

/**
 * A simple {@link Fragment} subclass.
 */
public class AttendanceFragment extends Fragment {

    /** Attendance recycler view */
    private RecyclerView recyclerView;

    /** Attendance list */
    private List<Attendance> attendanceList;

    /** Trainer text view */
    private TextView t_trainer;

    /** Attendance for type A recycler_training */
    private TextView t_attendanceA;

    /** Attendance for type B recycler_training */
    private TextView t_attendanceB;

    /** Attendance for type C recycler_training */
    private TextView t_attendanceC;

    /** Attendance for type D recycler_training */
    private TextView t_attendanceD;

    private Button btn_home;


    public AttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View attendanceView = inflater.inflate(R.layout.fragment_attendance, container, false);

        this.recyclerView   = attendanceView.findViewById(R.id.attendance_RV_table);
        this.attendanceList = new ArrayList<>();
        this.t_trainer      = attendanceView.findViewById(R.id.attendance_trainer_in_charge_name);
        this.t_attendanceA  = attendanceView.findViewById(R.id.attendance_training_labelA);
        this.t_attendanceB  = attendanceView.findViewById(R.id.attendance_training_labelB);
        this.t_attendanceC  = attendanceView.findViewById(R.id.attendance_training_labelC);
        this.t_attendanceD  = attendanceView.findViewById(R.id.attendance_training_labelD);

        this.btn_home       = attendanceView.findViewById(R.id.attendance_home_btn);

        this.btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.main_FL, new HomeFragment());
                transaction.commit();
            }
        });


        createAttendances();

        this.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        this.recyclerView.setAdapter(new AdapterAttendance(this.attendanceList));

        return attendanceView;
    }

    /**
     * Make attendances
     */
    public void createAttendances() {
        attendanceList.add(new Attendance(TrainingType.C, new Date()));
        for(int i = 0; i < 20; i++){
            if (i%2 == 0) this.attendanceList.add(new Attendance(TrainingType.A));
            else this.attendanceList.add(new Attendance(TrainingType.B));
        }

    }
}
