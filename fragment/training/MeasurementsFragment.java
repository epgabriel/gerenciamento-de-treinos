package ekelf.bodygymapp.fragment.training;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.fragment.HomeFragment;
import ekelf.bodygymapp.model.FitnessEvaluation;
import ekelf.bodygymapp.persistence.FitnessDAO;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeasurementsFragment extends Fragment {

    /** Right arm text view */
    private TextView tv_rightArm;

    /** Right forearm text view */
    private TextView tv_rightForearm;

    /** Right leg text view */
    private TextView tv_rightLeg;

    /** Right calf text view */
    private TextView tv_rightCalf;

    /** Shoulder text view */
    private TextView tv_shoulder;

    /** Chest text view */
    private TextView tv_chest;

    /** Hips text view */
    private TextView tv_hips;

    /** Waist text view */
    private TextView tv_waist;

    /** Left arm text view */
    private TextView tv_leftArm;

    /** Left forearm text view */
    private TextView tv_leftForearm;

    /** Left leg text view */
    private TextView tv_leftLeg;

    /** Left calf text view */
    private TextView tv_leftCalf;

    /** Home button */
    private Button btn_home;

    public MeasurementsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View measurementsView = inflater.inflate(R.layout.fragment_measurements, container, false);

        this.tv_rightArm      = measurementsView.findViewById(R.id.medidas_box_tv_right_arm_value);
        this.tv_rightForearm  = measurementsView.findViewById(R.id.medidas_box_tv_right_forearm_value);
        this.tv_rightLeg      = measurementsView.findViewById(R.id.medidas_box_tv_right_leg_value);
        this.tv_rightCalf     = measurementsView.findViewById(R.id.medidas_box_tv_right_calf_value);
        this.tv_shoulder      = measurementsView.findViewById(R.id.medidas_box_tv_shoulder_value);
        this.tv_chest         = measurementsView.findViewById(R.id.medidas_box_tv_chest_value);
        this.tv_hips          = measurementsView.findViewById(R.id.medidas_box_tv_hips_value);
        this.tv_waist         = measurementsView.findViewById(R.id.medidas_box_tv_waist_value);
        this.tv_leftArm       = measurementsView.findViewById(R.id.medidas_box_tv_left_arm_value);
        this.tv_leftForearm   = measurementsView.findViewById(R.id.medidas_box_tv_left_forearm_value);
        this.tv_leftLeg       = measurementsView.findViewById(R.id.medidas_box_tv_left_leg_value);
        this.tv_leftCalf      = measurementsView.findViewById(R.id.medidas_box_tv_left_calf_value);
        this.btn_home         = measurementsView.findViewById(R.id.medidas_home_button);
        this.btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.main_FL, new HomeFragment());
                transaction.commit();
            }
        });

        new FitnessDAO()
                .getReference()
                .orderByKey()
                .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .addListenerForSingleValueEvent(loadMeasurementsListener());

        return measurementsView;
    }



    private ValueEventListener loadMeasurementsListener() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.hasChildren()) {
                    dataSnapshot.getChildren()
                            .iterator()
                            .next()
                            .getRef()
                            .orderByChild("version")
                            .limitToLast(1)
                            .addListenerForSingleValueEvent(listenerFitnessExists());
                }

                else {
                    Toast.makeText(getContext(), "Não há avaliações cadastradas", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getContext(), "Não há avaliações cadastradas", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private ValueEventListener listenerFitnessExists() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    FitnessEvaluation fe = dataSnapshot.getChildren()
                            .iterator().next().getValue(FitnessEvaluation.class);

                    tv_rightArm.setText(fe.getRightArmFormatted());
                    tv_rightForearm.setText(fe.getRightForearmFormatted());
                    tv_rightLeg.setText(fe.getRightLegFormatted());
                    tv_rightCalf.setText(fe.getRightCalfFormatted());
                    tv_leftArm.setText(fe.getLeftArmFormatted());
                    tv_leftForearm.setText(fe.getLeftForearmFormatted());
                    tv_leftLeg.setText(fe.getLeftLegFormatted());
                    tv_leftCalf.setText(fe.getLeftCalfFormatted());
                    tv_shoulder.setText(fe.getShoulderFormatted());
                    tv_chest.setText(fe.getChestFormatted());
                    tv_hips.setText(fe.getHipFormatted());
                    tv_waist.setText(fe.getWaistFormatted());
                }

                else {
                    Toast.makeText(getContext(), "Não há avaliações cadastradas", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getContext(), "Não há avaliações cadastradas", Toast.LENGTH_SHORT).show();
            }
        };
    }


}
