package ekelf.bodygymapp.fragment.training;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.listener.RecyclerItemClickListener;
import ekelf.bodygymapp.adapter.AdapterWorkoutLog;
import ekelf.bodygymapp.model.exercise.ExerciseSet;
import ekelf.bodygymapp.model.training.TrainingModule;
import ekelf.bodygymapp.model.enums.TrainingType;
import ekelf.bodygymapp.model.exercise.WarmingUp;


/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutLogFragment extends Fragment {

    /**
     * Recycler views for recycler_trainings
     */
    private RecyclerView recyclerViewA, recyclerViewB, recyclerViewC,
            recyclerViewD, recyclerViewE;



    /**
     * TrainingModuleType List
     */
    private List<TrainingModule> trainingModuleList;



    /**
     * Text Views
     */
    private TextView titleTrainingA, titleTrainingB, titleTrainingC,
            titleTrainingD, titleTrainingE, pre_training_A, pre_training_B,
            pre_training_C, pre_training_D, pre_training_E, pos_training_A,
            pos_training_B, pos_training_C, pos_training_D, pos_training_E;



    public WorkoutLogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View wlogView = inflater.inflate(R.layout.fragment_workout_log, container, false);

        this.recyclerViewA   =  wlogView.findViewById(R.id.WorkoutLog_RView_A);
        this.recyclerViewB   =  wlogView.findViewById(R.id.WorkoutLog_RView_B);
        this.recyclerViewC   =  wlogView.findViewById(R.id.WorkoutLog_RView_C);
        this.recyclerViewD   =  wlogView.findViewById(R.id.WorkoutLog_RView_D);
        this.recyclerViewE   =  wlogView.findViewById(R.id.WorkoutLog_RView_E);

        this.trainingModuleList =  new ArrayList<>();

        this.titleTrainingA  =  wlogView.findViewById(R.id.WorkoutLog_title_A);
        this.titleTrainingB  =  wlogView.findViewById(R.id.WorkoutLog_title_B);
        this.titleTrainingC  =  wlogView.findViewById(R.id.WorkoutLog_title_C);
        this.titleTrainingD  =  wlogView.findViewById(R.id.WorkoutLog_title_D);
        this.titleTrainingE  =  wlogView.findViewById(R.id.WorkoutLog_title_E);

        this.pre_training_A =  wlogView.findViewById(R.id.WorkoutLog_warmingUpA);
        this.pre_training_B =  wlogView.findViewById(R.id.WorkoutLog_warmingUpB);
        this.pre_training_C =  wlogView.findViewById(R.id.WorkoutLog_warmingUpC);
        this.pre_training_D =  wlogView.findViewById(R.id.WorkoutLog_warmingUpD);
        this.pre_training_E =  wlogView.findViewById(R.id.WorkoutLog_warmingUpE);

        this.pos_training_A = wlogView.findViewById(R.id.WorkoutLog_trainingEndA);
        this.pos_training_B = wlogView.findViewById(R.id.WorkoutLog_trainingEndB);
        this.pos_training_C = wlogView.findViewById(R.id.WorkoutLog_trainingEndC);
        this.pos_training_D = wlogView.findViewById(R.id.WorkoutLog_trainingEndD);
        this.pos_training_E = wlogView.findViewById(R.id.WorkoutLog_trainingEndE);

        createExercise();

        /* Verifies which layouts should be displayed */
        for (TrainingModule t : this.trainingModuleList) {
            if      (t.getType() == TrainingType.A) wlogView.findViewById(R.id.WorkoutLogA_CL).setVisibility(View.VISIBLE);
            else if (t.getType() == TrainingType.B) wlogView.findViewById(R.id.WorkoutLogB_CL).setVisibility(View.VISIBLE);
            else if (t.getType() == TrainingType.C) wlogView.findViewById(R.id.WorkoutLogC_CL).setVisibility(View.VISIBLE);
            else if (t.getType() == TrainingType.D) wlogView.findViewById(R.id.WorkoutLogD_CL).setVisibility(View.VISIBLE);
            else if (t.getType() == TrainingType.E) wlogView.findViewById(R.id.WorkoutLogE_CL).setVisibility(View.VISIBLE);
        }


        //Configurar Recyclerview
        this.recyclerViewA.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.recyclerViewA.setHasFixedSize(true);
        this.recyclerViewA.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        this.recyclerViewA.setAdapter(new AdapterWorkoutLog(this.trainingModuleList.get(0).getExerciseSetList()));

        this.recyclerViewB.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.recyclerViewB.setHasFixedSize(true);
        this.recyclerViewB.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        this.recyclerViewB.setAdapter(new AdapterWorkoutLog(this.trainingModuleList.get(1).getExerciseSetList()));

        this.recyclerViewC.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.recyclerViewC.setHasFixedSize(true);
        this.recyclerViewC.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        this.recyclerViewC.setAdapter(new AdapterWorkoutLog(this.trainingModuleList.get(2).getExerciseSetList()));

        this.recyclerViewD.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.recyclerViewD.setHasFixedSize(true);
        this.recyclerViewD.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        this.recyclerViewD.setAdapter(new AdapterWorkoutLog(this.trainingModuleList.get(3).getExerciseSetList()));

        this.recyclerViewE.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.recyclerViewE.setHasFixedSize(true);
        this.recyclerViewE.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        this.recyclerViewE.setAdapter(new AdapterWorkoutLog(this.trainingModuleList.get(4).getExerciseSetList()));

        //evento de click
        this.recyclerViewA.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getActivity(),
                        this.recyclerViewA,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                ExerciseSet exerciseSet = trainingModuleList.get(0).getExerciseSetList().get(position);
                                Toast.makeText(
                                        getActivity(),
                                        "Item pressionado: " + exerciseSet.exerciseName(),
                                        Toast.LENGTH_SHORT
                                ).show();
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                ExerciseSet exerciseSet = trainingModuleList.get(0).getExerciseSetList().get(position);
                                Toast.makeText(
                                        getActivity(),
                                        "Click Longo: " + exerciseSet.exerciseName(),
                                        Toast.LENGTH_SHORT
                                ).show();
                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        }
                )
        );

        return wlogView;
    }

    public void createExercise(){

        this.trainingModuleList.add(
                new TrainingModule(
                        TrainingType.A,
                        "Superiores",
                        new WarmingUp("Esteira", (short) 10, (short) 5.4),
                        new ArrayList<ExerciseSet>()
                )
        );

        this.trainingModuleList.add(
                new TrainingModule(
                        TrainingType.B,
                        "Inferiores",
                        new WarmingUp("Bicicleta", (short) 10, (short) 6.7),
                        new ArrayList<ExerciseSet>()
                )
        );

        this.trainingModuleList.add(
                new TrainingModule(
                        TrainingType.C,
                        "Ombros",
                        new WarmingUp("Esteira", (short) 10, (short) 5.4),
                        new ArrayList<ExerciseSet>()
                )
        );

        this.trainingModuleList.add(
                new TrainingModule(
                        TrainingType.D,
                        "Costas",
                        new WarmingUp("Bicicleta", (short) 10, (short) 6.7),
                        new ArrayList<ExerciseSet>()
                )
        );

        this.trainingModuleList.add(
                new TrainingModule(
                        TrainingType.E,
                        "Extra",
                        new WarmingUp("Esteira", (short) 10, (short) 6.7),
                        new ArrayList<ExerciseSet>()
                )
        );

        this.trainingModuleList.get(0).getExerciseSetList().add(new ExerciseSet("Supino Inclinado", (short) 4, (short)15, (short)20, (short) 45));
        this.trainingModuleList.get(0).getExerciseSetList().add(new ExerciseSet("Peitoral Voador", (short) 4, (short)15, (short)25, (short) 45));
        this.trainingModuleList.get(0).getExerciseSetList().add(new ExerciseSet("Flexão de Braços", (short) 4, (short)20, (short)0,(short) 45));

        this.trainingModuleList.get(1).getExerciseSetList().add(new ExerciseSet("Leg Press 45°", (short) 4, (short)15, (short)20, (short) 45));
        this.trainingModuleList.get(1).getExerciseSetList().add(new ExerciseSet("Agachamento com Halter", (short) 4, (short)25, (short)5, (short) 45));
        this.trainingModuleList.get(1).getExerciseSetList().add(new ExerciseSet("Afundo com Barra", (short) 4, (short)15, (short)30, (short) 45));

        this.trainingModuleList.get(2).getExerciseSetList().add(new ExerciseSet("Desenvolvimento", (short) 4, (short)15, (short)10, (short) 45));
        this.trainingModuleList.get(2).getExerciseSetList().add(new ExerciseSet("Levantamento Lateral", (short) 4, (short)15, (short)20, (short) 45));

        this.trainingModuleList.get(3).getExerciseSetList().add(new ExerciseSet("Remada Curvada", (short) 4, (short)15, (short)20, (short) 45));

        this.trainingModuleList.get(4).getExerciseSetList().add(new ExerciseSet("Circuito Completo", (short) 4, (short)15, (short)20, (short) 45));



        this.titleTrainingA.setText(
                String.format(
                        "%s - %s",
                        this.trainingModuleList.get(0).getType().name(),
                        this.trainingModuleList.get(0).getDescription()
                )
        );

        this.titleTrainingB.setText(
                String.format(
                        "%s - %s",
                        this.trainingModuleList.get(1).getType().name(),
                        this.trainingModuleList.get(1).getDescription()
                )
        );

        this.titleTrainingC.setText(
                String.format(
                        "%s - %s",
                        this.trainingModuleList.get(2).getType().name(),
                        this.trainingModuleList.get(2).getDescription()
                )
        );

        this.titleTrainingD.setText(
                String.format(
                        "%s - %s",
                        this.trainingModuleList.get(3).getType().name(),
                        this.trainingModuleList.get(3).getDescription()
                )
        );

        this.titleTrainingE.setText(
                String.format(
                        "%s - %s",
                        this.trainingModuleList.get(4).getType().name(),
                        this.trainingModuleList.get(4).getDescription()
                )
        );



        this.pre_training_A.setText(
                this.trainingModuleList.get(0).getWarmingUp().toString(getActivity())
        );

        this.pre_training_B.setText(
                this.trainingModuleList.get(1).getWarmingUp().toString(getActivity())
        );

        this.pre_training_C.setText(
                this.trainingModuleList.get(2).getWarmingUp().toString(getActivity())
        );

        this.pre_training_D.setText(
                this.trainingModuleList.get(3).getWarmingUp().toString(getActivity())
        );
        this.pre_training_E.setText(
                this.trainingModuleList.get(4).getWarmingUp().toString(getActivity())
        );



        this.pos_training_A.setText(
                this.trainingModuleList.get(0).getPosTraining().toString(getActivity())
        );

        this.pos_training_B.setText(
                this.trainingModuleList.get(1).getPosTraining().toString(getActivity())
        );

        this.pos_training_C.setText(
                this.trainingModuleList.get(2).getPosTraining().toString(getActivity())
        );

        this.pos_training_D.setText(
                this.trainingModuleList.get(3).getPosTraining().toString(getActivity())
        );

        this.pos_training_E.setText(
                this.trainingModuleList.get(4).getPosTraining().toString(getActivity())
        );

    }

}
