package ekelf.bodygymapp.fragment.register.users;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.text.SimpleDateFormat;
import java.util.Date;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.management.BGManager;
import ekelf.bodygymapp.model.actors.User;
import ekelf.bodygymapp.model.enums.Gender;
import ekelf.bodygymapp.model.enums.Status;
import ekelf.bodygymapp.persistence.UserDAO;

/**
 * A simple {@link Fragment} subclass.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-14
 */
public class UserRegisterFragment extends Fragment {

    private TextInputLayout
            inputName, inputRegistration, inputFitnessEv,
            inputMedicalEx, inputLogin, inputTrainer;

    private RadioGroup
            radioGroupGender, radioGroupStatus;

    private FirebaseAuth userAuth;

    private User user;

    public UserRegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View userRegsView = inflater.inflate(R.layout.fragment_user_register, container, false);



        /* Links Text Inputs Layouts */
        this.inputName          =   userRegsView.findViewById(R.id.userRegs_input_name);
        this.inputRegistration  =   userRegsView.findViewById(R.id.userRegs_input_registration);
        this.inputFitnessEv     =   userRegsView.findViewById(R.id.userRegs_input_fitnessEv);
        this.inputMedicalEx     =   userRegsView.findViewById(R.id.userRegs_input_medicalEx);
        this.inputLogin         =   userRegsView.findViewById(R.id.userRegs_input_login);
        this.inputTrainer       =   userRegsView.findViewById(R.id.userRegs_input_trainer);

        /* Links RadioGroups */
        this.radioGroupGender   =   userRegsView.findViewById(R.id.userRegs_radioGroup_gender);
        this.radioGroupStatus   =   userRegsView.findViewById(R.id.userRegs_radioGroup_status);


        this.user = new User();
        this.user.setGender(Gender.F);
        this.user.setStatus(Status.ACTIVE);

        /* Handles with gender radio group events */
        this.radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if ( checkedId == R.id.userRegs_radioBtn_female)   user.setGender(Gender.F);

                else if (checkedId == R.id.userRegs_radioBtn_male ) user.setGender(Gender.M);

                else user.setGender(Gender.O);
            }
        });


        /* Handles with status radio group events */
        this.radioGroupStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.userRegs_radioBtn_active) user.setStatus(Status.ACTIVE);

                else if (checkedId == R.id.userRegs_radioBtn_inactive) user.setStatus(Status.INACTIVE);

                else user.setStatus(Status.VOID);
            }
        });



        /* Triggers registration button when clicked */
        userRegsView.findViewById(R.id.userRegs_btnRegistration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Verifies if all needed information was given */
                if ( ! inputName.getEditText().getText().toString().isEmpty()
                        && ! inputRegistration.getEditText().getText().toString().isEmpty()
                        && ! inputLogin.getEditText().getText().toString().isEmpty()
                        && ! inputTrainer.getEditText().getText().toString().isEmpty()
                ){
                    user.setName(inputName.getEditText().getText().toString());
                    user.setRegistration( Long.parseLong(
                            inputRegistration.getEditText().getText().toString()
                    ));
                    user.setEmail(inputLogin.getEditText().getText().toString() + getResources().getString(R.string.app_email_format));
                    user.setFitness_ev_expiration(inputFitnessEv.getEditText().getText().toString());
                    user.setMedical_ex_expiration(inputMedicalEx.getEditText().getText().toString());
                    user.setRegs_date(new SimpleDateFormat("dd/MM/yy")
                            .format(new Date())
                    );
                    user.setTrainer_regs(Integer.parseInt(
                            inputTrainer.getEditText().getText().toString()
                    ));

                    /* Verifies if there is internet connection in order to attempt the register */
                    if (! BGManager.getInstance().isConnected(getContext())) {
                        Toast.makeText(
                                getContext(),
                                "Error! Missing Connection",
                                Toast.LENGTH_SHORT
                        ).show();
                        return;
                    }

                    /* Try register user */
                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                            user.getEmail(),
                            String.valueOf( user.getRegistration() )
                    ).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if ( task.isSuccessful() ){

                                /* Registers the user in the server */
                                UserDAO userDAO = new UserDAO();
                                userDAO.writeObject(task.getResult().getUser().getUid(), user.toMap());

                                resetInputs();

                                Toast.makeText(getContext(), "Usuario cadastrado com sucesso", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(getContext(), task.getResult().toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }

                else {
                    Toast.makeText(
                            getContext(),
                            getResources().getString(R.string.error_userRegs_missing),
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }
        });

        return userRegsView;
    }

    public void resetInputs() {
        /* Links Text Inputs Layouts */
        this.inputName.getEditText().getText().clear();
        this.inputRegistration.getEditText().getText().clear();
        this.inputFitnessEv.getEditText().getText().clear();
        this.inputMedicalEx.getEditText().getText().clear();
        this.inputLogin.getEditText().getText().clear();
        this.inputTrainer.getEditText().getText().clear();

        this.inputName.setSelected(true);

    }

}
