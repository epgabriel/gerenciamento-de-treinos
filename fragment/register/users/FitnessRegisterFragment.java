package ekelf.bodygymapp.fragment.register.users;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;


import ekelf.bodygymapp.R;
import ekelf.bodygymapp.fragment.register.Registrable;
import ekelf.bodygymapp.model.FitnessEvaluation;
import ekelf.bodygymapp.model.actors.User;
import ekelf.bodygymapp.persistence.FitnessDAO;
import ekelf.bodygymapp.persistence.UserDAO;

/**
 * A simple {@link Fragment} subclass.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-27
 */
public class FitnessRegisterFragment extends Fragment implements Registrable {


    private TextInputLayout
            inputWeight, inputGoal, inputHeight, inputUserRegister,
            inputFatRate, inputRightArm, inputLeftArm,
            inputRightForearm, inputLeftForearm,
            inputRightLeg, inputLeftLeg,
            inputRightCalf, inputLeftCalf,
            inputShoulder, inputChest,
            inputHip, inputWaist;

    private FitnessEvaluation fitnessEvaluation;

    private ProgressBar progressBar;


    public FitnessRegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fitnessEvView =  inflater.inflate(R.layout.fragment_fitness_register, container, false);

        /* Link views */
        this.inputUserRegister = fitnessEvView.findViewById(R.id.fitnessEv_input_user);
        this.inputWeight = fitnessEvView.findViewById(R.id.fitnessEv_input_weight);
        this.inputGoal = fitnessEvView.findViewById(R.id.fitnessEv_input_goal);
        this.inputHeight = fitnessEvView.findViewById(R.id.fitnessEv_input_height);
        this.inputFatRate= fitnessEvView.findViewById(R.id.fitnessEv_input_fatRate);
        this.inputRightArm = fitnessEvView.findViewById(R.id.fitnessEv_input_arm_right);
        this.inputLeftArm = fitnessEvView.findViewById(R.id.fitnessEv_input_arm_left);
        this.inputRightForearm = fitnessEvView.findViewById(R.id.fitnessEv_input_forearm_right);
        this.inputLeftForearm = fitnessEvView.findViewById(R.id.fitnessEv_input_forearm_left);
        this.inputRightLeg = fitnessEvView.findViewById(R.id.fitnessEv_input_leg_right);
        this.inputLeftLeg = fitnessEvView.findViewById(R.id.fitnessEv_input_leg_left);
        this.inputRightCalf = fitnessEvView.findViewById(R.id.fitnessEv_input_calf_right);
        this.inputLeftCalf = fitnessEvView.findViewById(R.id.fitnessEv_input_calf_left);
        this.inputShoulder = fitnessEvView.findViewById(R.id.fitnessEv_input_shoulder);
        this.inputChest = fitnessEvView.findViewById(R.id.fitnessEv_input_chest);
        this.inputHip = fitnessEvView.findViewById(R.id.fitnessEv_input_hip);
        this.inputWaist = fitnessEvView.findViewById(R.id.fitnessEv_input_waist);

        this.progressBar = fitnessEvView.findViewById(R.id.fitnessRegs_progressBar);
        this.progressBar.setVisibility(View.GONE);

        this.fitnessEvaluation = new FitnessEvaluation();

        fitnessEvView.findViewById(R.id.fitnessEv_btnHome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getActivity().getSupportFragmentManager().beginTransaction().remove(FitnessEvRegisterFragment.this).commit();
                getActivity().onBackPressed();
            }
        });

        fitnessEvView.findViewById(R.id.fitnessEv_btnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Starts registration process, show progress bar
                showProgressBar();

                // Verifies if the form was properly filled. If not, then the process shall ends
                if (!validForm()) {
                    finalizeNotFilled();
                    return;
                }

                // Once the form is validated, tries to register the evaluation.
                // 1st - Verifies if the user exists;
                // 2nd - Attempts to register user's fitness evaluation.
                new UserDAO().getReference()
                        .orderByChild("registration").equalTo(Long.parseLong(inputUserRegister.getEditText().getText().toString()))
                        .addListenerForSingleValueEvent(userSeekerListener());
            }
        });

        return fitnessEvView;
    }

    /**
     *
     * @return
     */
    private ValueEventListener userSeekerListener() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String key = null;
                for (DataSnapshot ds : dataSnapshot.getChildren()) key = ds.getKey();

                // If key is null, then there's no user register with the given id.
                // Shows error message and finalizes process with "user not found" error
                if(key == null){
                   finalizeWithFailure(getContext().getResources().getString(R.string.error_user_not_found));
                    return;
                }

                // Keeps the user found with the given id
                final User user = dataSnapshot.child(key).getValue(User.class);

                // If the retrieved user is equal to null, then there's invalid information. Stop process.
                // But, if not, proceeds to final step of registration.
                if (user != null) {
                    FitnessDAO fitnessDAO = new FitnessDAO();
                    fitnessDAO.getReference().child(key).orderByChild("version").limitToLast(1)
                            .addListenerForSingleValueEvent(fitnessRegsListener(key));
                } else {
                    inputUserRegister.setErrorEnabled(true);
                    inputUserRegister.setError("id inválido");
                    finalizeWithFailure(getContext().getResources().getString(R.string.error_user_not_found));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finalizeWithFailure(databaseError.getMessage());
            }
        };
    }



    /**
     * @param uid
     * @return Listener for Fitness Evaluation Registration
     */
    private ValueEventListener fitnessRegsListener(final String uid) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // Calculates evaluation's version based on the last evaluation, if it exists.
                // If it does not exists, sets version to 1.
                int version = dataSnapshot.getChildren().iterator().hasNext()  ?
                              dataSnapshot.getChildren().iterator()
                                .next().getValue(FitnessEvaluation.class)
                                .getVersion() + 1 : 1;

                // Sets version to fitness evaluation object
                fitnessEvaluation.setVersion(version);

                // Starts a new Fitness Evaluation Data Access Object for write the evaluation
                new FitnessDAO().writeObject(uid, fitnessEvaluation.toMap())
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                finalizeWithSuccess();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                finalizeWithFailure(e.getMessage());
                            }
                        });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finalizeWithFailure(databaseError.getMessage());
            }
        };
    }

    /** Shows the progress bar - some process has been started */
    private final void showProgressBar() { progressBar.setVisibility(View.VISIBLE); }


    /* Registrable methods */
    @Override
    public void finalizeNotFilled() {
        Toast.makeText(
                getContext(),
                "Preencha todos os dados",
                Toast.LENGTH_SHORT
        ).show();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void finalizeWithSuccess() {
        Toast.makeText(
                getContext(),
                "Avaliação cadastrada com sucesso",
                Toast.LENGTH_SHORT
        ).show();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void finalizeWithFailure(String e){
        Toast.makeText(
                getContext(),
                "Erro ao cadastrar avaliação: " + e,
                Toast.LENGTH_SHORT
        ).show();
        progressBar.setVisibility(View.GONE);
    }



    /**
     * Validates the form, setting fitness evaluation information
     * @return either the form is valid or not
     */
    public boolean validForm() {
        boolean filled = true;

        // Checks User Id
        if (inputUserRegister.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputUserRegister.setErrorEnabled(true);
            inputUserRegister.setError("Digite um id de usuário");
        }
        else{
            inputUserRegister.setError(null);
        }


        // Checks Weight
        if (inputWeight.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputWeight.setErrorEnabled(true);
            inputWeight.setError("Digite um peso(kgs)");
        }else {
            inputWeight.setError(null);
            fitnessEvaluation.setWeight(
                    Float.parseFloat(
                            inputWeight.getEditText().getText().toString()
                    )
            );
        }


        // Checks Goal
        if (inputGoal.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputGoal.setErrorEnabled(true);
            inputGoal.setError("Digite um objetivo(kgs)");
        }else {
            inputGoal.setError(null);
            fitnessEvaluation.setGoal(
                    Float.parseFloat(
                            inputGoal.getEditText().getText().toString()
                    )
            );
        }


        // Checks Height
        if (inputHeight.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputHeight.setErrorEnabled(true);
            inputHeight.setError("Digite uma altura(cms)");
        }else {
            inputHeight.setError(null);
            fitnessEvaluation.setHeight(
                    Float.parseFloat(
                            inputHeight.getEditText().getText().toString()
                    )
            );
        }


        // Checks Fat Rate
        if (inputFatRate.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputFatRate.setErrorEnabled(true);
            inputFatRate.setError("Digite uma taxa (%) de gordura");
        }else {
            inputFatRate.setError(null);
            fitnessEvaluation.setFatRate(
                    Float.parseFloat(
                            inputFatRate.getEditText().getText().toString()
                    )
            );
        }


        // Checks Right Arm
        if (inputRightArm.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputRightArm.setErrorEnabled(true);
            inputRightArm.setError("Digite uma medida (cms)");
        }else {
            inputRightArm.setError(null);
            fitnessEvaluation.setRightArm(
                    Float.parseFloat(
                            inputRightArm.getEditText().getText().toString()
                    )
            );
        }


        // Checks Left Arm
        if (inputLeftArm.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputLeftArm.setErrorEnabled(true);
            inputLeftArm.setError("Digite uma medida (cms)");
        }else {
            inputLeftArm.setError(null);
            fitnessEvaluation.setLeftArm(
                    Float.parseFloat(
                            inputLeftArm.getEditText().getText().toString()
                    )
            );
        }


        // Checks Right Forearm
        if (inputRightForearm.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputRightForearm.setErrorEnabled(true);
            inputRightForearm.setError("Digite uma medida (cms)");
        }else {
            inputRightForearm.setError(null);
            fitnessEvaluation.setRightForearm(
                    Float.parseFloat(
                            inputRightForearm.getEditText().getText().toString()
                    )
            );
        }


        // Checks Left Forearm
        if (inputLeftForearm.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputLeftForearm.setErrorEnabled(true);
            inputLeftForearm.setError("Digite uma medida (cms)");
        }else {
            inputLeftForearm.setError(null);
            fitnessEvaluation.setLeftForearm(
                    Float.parseFloat(
                            inputLeftForearm.getEditText().getText().toString()
                    )
            );
        }


        // Checks Right Leg
        if (inputRightLeg.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputRightLeg.setErrorEnabled(true);
            inputRightLeg.setError("Digite uma medida (cms)");
        }else {
            inputRightLeg.setError(null);
            fitnessEvaluation.setRightLeg(
                    Float.parseFloat(
                            inputRightLeg.getEditText().getText().toString()
                    )
            );
        }


        // Checks Left Leg
        if (inputLeftLeg.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputLeftLeg.setErrorEnabled(true);
            inputLeftLeg.setError("Digite uma medida (cms)");
        }else {
            inputLeftLeg.setError(null);
            fitnessEvaluation.setLeftLeg(
                    Float.parseFloat(
                            inputLeftLeg.getEditText().getText().toString()
                    )
            );
        }


        // Checks Right Calf
        if (inputRightCalf.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputRightCalf.setErrorEnabled(true);
            inputRightCalf.setError("Digite uma medida (cms)");
        }else {
            inputRightCalf.setError(null);
            fitnessEvaluation.setRightCalf(
                    Float.parseFloat(
                            inputRightCalf.getEditText().getText().toString()
                    )
            );
        }


        // Checks Left Calf
        if (inputLeftCalf.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputLeftCalf.setErrorEnabled(true);
            inputLeftCalf.setError("Digite uma medida (cms)");
        }else {
            inputLeftCalf.setError(null);
            fitnessEvaluation.setLeftCalf(
                    Float.parseFloat(
                            inputLeftCalf.getEditText().getText().toString()
                    )
            );
        }


        // Checks Shoulder
        if (inputShoulder.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputShoulder.setErrorEnabled(true);
            inputShoulder.setError("Digite uma medida (cms)");
        }else {
            inputShoulder.setError(null);
            fitnessEvaluation.setShoulder(
                    Float.parseFloat(
                            inputShoulder.getEditText().getText().toString()
                    )
            );
        }


        // Checks Chest
        if (inputChest.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputChest.setErrorEnabled(true);
            inputChest.setError("Digite uma medida (cms)");
        }else {
            inputChest.setError(null);
            fitnessEvaluation.setChest(
                    Float.parseFloat(
                            inputChest.getEditText().getText().toString()
                    )
            );
        }


        // Checks Hip
        if (inputHip.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputHip.setErrorEnabled(true);
            inputHip.setError("Digite uma medida (cms)");
        }else {
            inputHip.setError(null);
            fitnessEvaluation.setHip(
                    Float.parseFloat(
                            inputHip.getEditText().getText().toString()
                    )
            );
        }


        // Checks Waist
        if (inputWaist.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputWaist.setErrorEnabled(true);
            inputWaist.setError("Digite uma medida (cms)");
        }else {
            inputWaist.setError(null);
            fitnessEvaluation.setWaist(
                    Float.parseFloat(
                            inputWaist.getEditText().getText().toString()
                    )
            );
        }

        return filled;
    }
}
