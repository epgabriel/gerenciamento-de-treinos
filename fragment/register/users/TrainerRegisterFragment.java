package ekelf.bodygymapp.fragment.register.users;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.model.actors.Trainer;
import ekelf.bodygymapp.model.enums.EmployeePosition;
import ekelf.bodygymapp.model.enums.Gender;
import ekelf.bodygymapp.model.enums.Status;
import ekelf.bodygymapp.persistence.UserDAO;

/**
 * A simple {@link Fragment} subclass.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-03-14
 */
public class TrainerRegisterFragment extends Fragment {

    private TextInputLayout
            inputName, inputRegistration, inputLogin;

    private RadioGroup
            radioGroupGender, radioGroupStatus, radioGroupPosition;

    private CheckBox
            checkMorning, checkAfternoon, checkEvening;

    private Trainer trainer;


    public TrainerRegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View trainerRegsView = inflater.inflate(R.layout.fragment_trainer_register, container, false);

        /* Links Text Inputs Layouts */
        this.inputName          =   trainerRegsView.findViewById(R.id.trainerRegs_input_name);
        this.inputRegistration  =   trainerRegsView.findViewById(R.id.trainerRegs_input_registration);
        this.inputLogin         =   trainerRegsView.findViewById(R.id.trainerRegs_input_login);

        /* Links RadioGroups */
        this.radioGroupGender   =   trainerRegsView.findViewById(R.id.trainerRegs_radioGroup_gender);
        this.radioGroupStatus   =   trainerRegsView.findViewById(R.id.trainerRegs_radioGroup_status);
        this.radioGroupPosition =   trainerRegsView.findViewById(R.id.trainerRegs_radioGroup_position);

        /* Links CheckBoxs */
        this.checkMorning   = trainerRegsView.findViewById(R.id.trainerRegs_morning_shift);
        this.checkAfternoon = trainerRegsView.findViewById(R.id.trainerRegs_afternoon_shift);
        this.checkEvening   = trainerRegsView.findViewById(R.id.trainerRegs_evening_shift);

        /* Constructs Trainer Object */
        this.trainer = new Trainer();
        this.trainer.setGender(Gender.F);
        this.trainer.setStatus(Status.ACTIVE);
        this.trainer.setPosition(EmployeePosition.TRAINER);

        /* Handles with gender radio group events - gender*/
        this.radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if ( checkedId == R.id.trainerRegs_radioBtn_female)   trainer.setGender(Gender.F);
                else if (checkedId == R.id.trainerRegs_radioBtn_male ) trainer.setGender(Gender.M);
                else trainer.setGender(Gender.O);
            }
        });


        /* Handles with status radio group events - status*/
        this.radioGroupStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.trainerRegs_radioBtn_active) trainer.setStatus(Status.ACTIVE);
                else if (checkedId == R.id.trainerRegs_radioBtn_inactive) trainer.setStatus(Status.INACTIVE);
                else trainer.setStatus(Status.VOID);
            }
        });

        /* Handles with status radio group events - position */
        this.radioGroupPosition.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.trainerRegs_radioBtn_trainer)trainer.setPosition(EmployeePosition.TRAINER);
                else trainer.setPosition(EmployeePosition.INTERN);
            }
        });



        /* Triggers registration button when clicked */
        trainerRegsView.findViewById(R.id.trainerRegs_btnRegistration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /* Verifies if the admin didn't select any shift */
                if ( ! checkMorning.isChecked()
                        && ! checkAfternoon.isChecked()
                        && !checkEvening.isChecked()
                ) {
                    /*If it is so, then show a missing message and stops the function*/
                    Toast.makeText(
                            getContext(),
                            getResources().getString(R.string.error_missing_shift),
                            Toast.LENGTH_SHORT
                    ).show();

                    return;
                }

                /* At least one shift is selected, so set them to the Trainer object */
                trainer.setShift(0, checkMorning.isChecked());
                trainer.setShift(1, checkAfternoon.isChecked());
                trainer.setShift(2, checkEvening.isChecked());

                /*Verifies if all needed information was given */
                if ( ! inputName.getEditText().getText().toString().isEmpty()
                        && ! inputRegistration.getEditText().getText().toString().isEmpty()
                        && ! inputLogin.getEditText().getText().toString().isEmpty()
                ) {
                    trainer.setName(inputName.getEditText().getText().toString());
                    trainer.setRegistration(Integer.parseInt(
                            inputRegistration.getEditText().getText().toString()
                    ));
                    trainer.setEmail(inputLogin.getEditText().getText().toString() + getResources().getString(R.string.app_email_format));



                    trainer.setRegistration(Long.parseLong(inputRegistration.getEditText().getText().toString()));
                    trainer.setTrainer_regs(trainer.getRegistration());
                    trainer.setName(inputName.getEditText().getText().toString());
                    trainer.setEmail(inputLogin.getEditText().getText().toString() + getResources().getString(R.string.app_email_format));


                    /* Try register user */
                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                            trainer.getEmail(),
                            String.valueOf( trainer.getRegistration() )
                    ).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if ( task.isSuccessful() ){

                                /* Registers the user in the server */
                                UserDAO userDAO = new UserDAO();
                                userDAO.writeObject(task.getResult().getUser().getUid(), trainer.toMap());

                                Toast.makeText(
                                        getContext(),
                                        "Professor cadastrado com sucesso",
                                        Toast.LENGTH_SHORT
                                ).show();

                                resetInputs();
                            }
                            else {
                                Toast.makeText(
                                        getContext(),
                                        "Falha ao cadastrar professor",
                                        Toast.LENGTH_SHORT
                                ).show();
                            }
                        }
                    });

                }

                /* If all needed information wasn't given, then shows Toast with Missing Error message */
                else {
                    Toast.makeText(
                            getContext(),
                            getResources().getString(R.string.error_trainerRegs_missing),
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }
        });

        return trainerRegsView;
    }


    public void resetInputs() {
        /* Clear Text Inputs Layouts */
        this.inputName.getEditText().getText().clear();
        this.inputRegistration.getEditText().getText().clear();
        this.inputLogin.getEditText().getText().clear();


        /* Clear CheckBoxs */
        this.checkMorning.setChecked(false);
        this.checkAfternoon.setChecked(false);
        this.checkEvening.setChecked(false);

        /*Select name */
        this.inputName.setSelected(true);
    }
}
