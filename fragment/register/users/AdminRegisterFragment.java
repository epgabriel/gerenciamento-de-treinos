package ekelf.bodygymapp.fragment.register.users;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.model.actors.Admin;
import ekelf.bodygymapp.persistence.UserDAO;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdminRegisterFragment extends Fragment {

    private TextInputLayout inputName, inputEmail, inputId;


    public AdminRegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View adminView = inflater.inflate(R.layout.fragment_admin_register, container, false);

        this.inputName = adminView.findViewById(R.id.admin_input_name);
        this.inputId   = adminView.findViewById(R.id.admin_input_id);
        this.inputEmail= adminView.findViewById(R.id.admin_input_email);

        /* Register Admin */
        adminView.findViewById(R.id.admin_btn_register)
                .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { registerAdmin(v); }
        });

        return adminView;
    }



    public void registerAdmin(View v) {
        boolean filled = true;

        if (inputName.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputName.setError("Preencha esse campo");
        }

        if (inputEmail.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputEmail.setError("Preencha esse campo");
        }


        if (inputId.getEditText().getText().toString().isEmpty()) {
            filled = false;
            inputId.setError("Preencha esse campo");
        }
        else if(inputId.getEditText().getText().toString().length() < 6){
            filled = false;
            inputId.setError("Pelo menos 6 caracteres");
        }

        if (!filled) return;

        final Admin user = new Admin(
                Long.parseLong(inputId.getEditText().getText().toString()),
                inputName.getEditText().getText().toString(),
                inputEmail.getEditText().getText().toString() + getResources().getString(R.string.app_email_format)
        );

        /* Try register user */
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(
                user.getEmail(),
                String.valueOf( user.getRegistration() )
        ).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if ( task.isSuccessful() ){

                    /* Registers the user in the server */
                    UserDAO userDAO = new UserDAO();
                    userDAO.writeObject(task.getResult().getUser().getUid(), user.toMap());

                    Toast.makeText(
                            getContext(),
                            "Administrador cadastrado com sucesso",
                            Toast.LENGTH_SHORT
                    ).show();

                    resetInputs();
                }
                else {

                    Toast.makeText(
                            getContext(),
                            "Falha ao cadastrar administrador",
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }
        });
    }

    public void resetInputs() {
        this.inputName.getEditText().getText().clear();
        this.inputId.getEditText().getText().clear();
        this.inputEmail.getEditText().getText().clear();

        this.inputName.setSelected(true);
    }


}
