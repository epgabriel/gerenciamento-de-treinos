package ekelf.bodygymapp.fragment.register;

public interface Registrable {

    /** Tells to the user that are fields not filled. */
    void finalizeNotFilled();

    /** Tells to the user that the registration was successful. */
    void finalizeWithSuccess();

    /**
     * Tells to the user that an error occurred during
     * registration attempt.
     *
     * @param e cause of failure
     */
    void finalizeWithFailure(String e);

    boolean validForm();
}
