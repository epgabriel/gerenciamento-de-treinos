package ekelf.bodygymapp.fragment.register.training;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.model.exercise.WarmingUp;

/**
 * A simple {@link Fragment} subclass.
 */
public class WarmingUpRegisterFragment extends Fragment {

    private TextInputLayout name, time, weight;

    private TrainingRegistrable dataPasser;

    public WarmingUpRegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (TrainingRegistrable) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View warmingView = inflater.inflate(R.layout.fragment_warming_up_register, container, false);

        name = warmingView.findViewById(R.id.warmingupRegs_input_name);
        time = warmingView.findViewById(R.id.warmingupRegs_input_time);
        weight = warmingView.findViewById(R.id.warmingupRegs_input_weight);


        // Saves warming up
        warmingView.findViewById(R.id.warmingupRegs_btn_save)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!validateForm()) return;

                        WarmingUp warmingUp = new WarmingUp(
                                name.getEditText().getText().toString(),
                                Integer.parseInt(time.getEditText().getText().toString()),
                                Float.parseFloat(weight.getEditText().getText().toString())
                        );

                        if (getArguments().getInt("warming") == 0) dataPasser.onWarmingUpRegistered(warmingUp, getModuleID());
                        else dataPasser.onPosTrainingRegistered(warmingUp, getModuleID());

                        getFragmentManager().beginTransaction().remove(WarmingUpRegisterFragment.this).commit();

                    }
                });

        return warmingView;
    }




    public int getModuleID() {

        char module = getArguments().getChar("Module");

        if (module == 'A') return 0;
        else if (module == 'B') return 1;
        else if (module == 'C') return 2;
        else if (module == 'D') return 3;
        else if (module == 'E') return 4;
        else if (module == 'F') return 5;
        return -1;

    }





    public boolean validateForm() {
        boolean valid = true;

        if (name.getEditText().getText().toString().isEmpty()) {
            valid = false;
            name.setErrorEnabled(true);
            name.setError(getContext().getResources().getString(R.string.error_required));
        }else name.setError(null);

        if (time.getEditText().getText().toString().isEmpty()) {
            valid = false;
            time.setErrorEnabled(true);
            time.setError(getContext().getResources().getString(R.string.error_required));
        }else time.setError(null);

        if (weight.getEditText().getText().toString().isEmpty()) {
            valid = false;
            weight.setErrorEnabled(true);
            weight.setError(getContext().getResources().getString(R.string.error_required));
        }else weight.setError(null);

        return valid;
    }
}
