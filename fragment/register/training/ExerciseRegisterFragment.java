package ekelf.bodygymapp.fragment.register.training;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.model.exercise.Exercise;
import ekelf.bodygymapp.model.exercise.ExerciseSet;
import ekelf.bodygymapp.model.exercise.Series;

/**
 * A simple {@link Fragment} subclass.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-20-03
 */
public class ExerciseRegisterFragment extends Fragment {

    /**
     * Text Views
     */
    private TextView tv_userInfo;


    /**
     * Radio Groups
     */
    private RadioGroup rg_type;


    /**
     * Text inputs layouts
     */
    private TextInputLayout
            inputName1, inputSets1, inputRepts1, inputWeight1, inputDropInd1,
            inputName2, inputWeight2, inputDropInd2, inputResting,
            inputName3, inputWeight3, inputDropInd3;


    /**
     * Dividers
     */
    private View divider2, divider3;


    /**
     * Buttons
     */
    private Button btn_save;

    /**
     * User identification
     */
    private String userTrainingID, userID;

    /**
     * Exercise Set model class
     */
    private ExerciseSet exerciseSet;


    private TrainingRegistrable dataPasser;


    /**
     * Default empty constructor required for fragments
     */
    public ExerciseRegisterFragment() {}


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (TrainingRegistrable) context;
    }

    /**
     * On create method. Creates, instantiates and links all the variables and initial configs
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View exRegsView = inflater.inflate(R.layout.fragment_exercise_register, container, false);


        /*
         * Links the views
         */
        this.tv_userInfo   =  exRegsView.findViewById(R.id.exerciseRegs_userInfo);
        this.rg_type       =  exRegsView.findViewById(R.id.exerciseRegs_radioGroup);

        this.inputName1    =  exRegsView.findViewById(R.id.warmingupRegs_input_name);
        this.inputSets1    =  exRegsView.findViewById(R.id.warmingupRegs_input_time);
        this.inputRepts1   =  exRegsView.findViewById(R.id.warmingupRegs_input_weight);
        this.inputWeight1  =  exRegsView.findViewById(R.id.exerciseRegs_input_weight_1);
        this.inputDropInd1 =  exRegsView.findViewById(R.id.exerciseRegs_dropInd_1);

        this.inputName2    =  exRegsView.findViewById(R.id.exerciseRegs_input_name_2);
        this.inputWeight2  =  exRegsView.findViewById(R.id.exerciseRegs_input_weight_2);
        this.inputDropInd2 =  exRegsView.findViewById(R.id.exerciseRegs_dropInd_2);

        this.inputName3    =  exRegsView.findViewById(R.id.exerciseRegs_input_name_3);
        this.inputWeight3  =  exRegsView.findViewById(R.id.exerciseRegs_input_weight_3);
        this.inputDropInd3 =  exRegsView.findViewById(R.id.exerciseRegs_dropInd_3);

        this.inputResting  = exRegsView.findViewById(R.id.exerciseRegs_resting);

        this.divider2 = exRegsView.findViewById(R.id.exerciseRegs_divider_2);
        this.divider3 = exRegsView.findViewById(R.id.exerciseRegs_divider_3);

        this.btn_save = exRegsView.findViewById(R.id.exerciseRegs_btn_save);

//        this.userTrainingID = getActivity().getIntent().getExtras().getString("bg-userTrainingID");
//        this.userID = getActivity().getIntent().getExtras().getString("bg-userID");
//        this.tv_userInfo.setText(getActivity().getIntent().getExtras().getString("bg-userInfo"));

        hideTriSet();
        hideBiSet();

        




        //Configs Radio Group Listener
        configExerciseRegistration(R.id.exerciseRegs_radio_oneSet);
        this.rg_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                configExerciseRegistration(checkedId);
            }
        });



        return exRegsView;
    }


    /**
     * Hide all bi-set oneSet views.
     */
    public void hideBiSet() {
        this.divider2.setVisibility(View.GONE);
        this.inputName2.setVisibility(View.GONE);
        this.inputWeight2.setVisibility(View.GONE);
        this.inputDropInd2.setVisibility(View.GONE);
    }


    /**
     * Hide all tri-set oneSet views.
     */
    public void hideTriSet() {
        this.divider3.setVisibility(View.GONE);
        this.inputName3.setVisibility(View.GONE);
        this.inputWeight3.setVisibility(View.GONE);
        this.inputDropInd3.setVisibility(View.GONE);
    }


    /**
     * Show all bi-set oneSet views.
     */
    public void showBiSet() {
        this.divider2.setVisibility(View.VISIBLE);
        this.inputName2.setVisibility(View.VISIBLE);
        this.inputWeight2.setVisibility(View.VISIBLE);
        this.inputDropInd2.setVisibility(View.VISIBLE);
    }

    /**
     * Show all tri-set oneSet views
     */
    public void showTriSet() {
        showBiSet();
        this.divider3.setVisibility(View.VISIBLE);
        this.inputName3.setVisibility(View.VISIBLE);
        this.inputWeight3.setVisibility(View.VISIBLE);
        this.inputDropInd3.setVisibility(View.VISIBLE);
    }



    /**
     * Validates the form, according to the radio button checked.
     * Level 1 validates One-set oneSet.
     * Level 2 validates One-set and Bi-set exercises.
     * Level 3 validates One-set, Bi-set and Tri-set exercises.
     *
     * Finally, verifies if the resting time was set.
     *
     * @param level level which level the function should validate
     * @return whether the form is valid or not
     */
    public boolean validateForm(int level) {

        boolean valid = true;

        /* If level is equal or greater then 1, then validates one-set oneSet */
        if ( level >= 1 ) {

            //Log.i("bb1", String.valueOf(valid));
            if (this.inputName1.getEditText().getText().toString().isEmpty()){
                this.inputName1.setError(getResources().getString(R.string.error_required));
                valid = false;
            }else this.inputName1.setError(null);

            if (this.inputSets1.getEditText().getText().toString().isEmpty()) {
                this.inputSets1.setError(getResources().getString(R.string.error_required));
                valid = false;
            }else this.inputSets1.setError(null);

            if (this.inputRepts1.getEditText().getText().toString().isEmpty()) {
                this.inputRepts1.setError(getResources().getString(R.string.error_required));
                valid = false;
            }else this.inputRepts1.setError(null);

            if (this.inputWeight1.getEditText().getText().toString().isEmpty()) {
                this.inputWeight1.setError(getResources().getString(R.string.error_required));
                valid = false;
            }else this.inputWeight1.setError(null);

            if (this.inputDropInd1.getEditText().getText().toString().isEmpty()) {
                this.inputDropInd1.setError(getResources().getString(R.string.error_required));
                valid = false;
            }else this.inputDropInd1.setError(null);
        }

        /* If level is equal or greater then 2, then validates bi-set oneSet */
        if ( level >= 2 ) {
            if (this.inputName2.getEditText().getText().toString().isEmpty()){
                this.inputName2.setError(getResources().getString(R.string.error_required));
                valid = false;
            }else this.inputName2.setError(null);

            if (this.inputDropInd2.getEditText().getText().toString().isEmpty()) {
                this.inputDropInd2.setError(getResources().getString(R.string.error_required));
                valid = false;
            }else this.inputDropInd2.setError(null);

            if (this.inputWeight2.getEditText().getText().toString().isEmpty()) {
                this.inputWeight2.setError(getResources().getString(R.string.error_required));
                valid = false;
            }else this.inputWeight2.setError(null);
        }



        /* If level is equal to 3, then validates tri-set oneSet */
        if ( level == 3 ) {
            if (this.inputName3.getEditText().getText().toString().isEmpty()){
                this.inputName3.setError(getResources().getString(R.string.error_required));
                valid = false;
            }else this.inputName3.setError(null);

            if (this.inputDropInd3.getEditText().getText().toString().isEmpty()) {
                this.inputDropInd3.setError(getResources().getString(R.string.error_required));
                valid = false;
            }else this.inputDropInd3.setError(null);

            if (this.inputWeight3.getEditText().getText().toString().isEmpty()) {
                this.inputWeight3.setError(getResources().getString(R.string.error_required));
                valid = false;
            }else this.inputWeight3.setError(null);
        }

        /* Checks resting time */
        if (this.inputResting.getEditText().getText().toString().isEmpty()){
            this.inputResting.setError(getResources().getString(R.string.error_required));
            valid = false;
        } else this.inputResting.setError(null);

        return valid;
    }

    /**
     * Builds a new OneSet from the params.
     *
     * @param name TextInputLayout
     * @param sets TextInputLayout
     * @param reps TextInputLayout
     * @param weight TextInputLayout
     * @param restingTime EditText
     * @return new ExerciseSet
     */
    public ExerciseSet buildExercise(
            TextInputLayout name,
            TextInputLayout sets,
            TextInputLayout reps,
            TextInputLayout weight,
            TextInputLayout restingTime,
            TextInputLayout dropInd
    ) {
        ExerciseSet exerciseSet = new ExerciseSet();
        exerciseSet.getExercise().setName(name.getEditText().getText().toString());
        exerciseSet.getSeries().setSets(Integer.parseInt(sets.getEditText().getText().toString()));
        exerciseSet.getSeries().setRepeat(Integer.parseInt(reps.getEditText().getText().toString()));
        exerciseSet.getExercise().setWeight(Integer.parseInt(weight.getEditText().getText().toString()));
        exerciseSet.getExercise().setDropSet_rate(Integer.parseInt(dropInd.getEditText().getText().toString()));
        exerciseSet.setRest_timing(Integer.parseInt(restingTime.getEditText().getText().toString()));
        return exerciseSet;
    }

    /**
     * Builds a simple Exercise object from the params
     *
     * @param name
     * @param weight
     * @param dropInd
     * @return
     */
    public Exercise buildExercise(
            TextInputLayout name,
            TextInputLayout weight,
            TextInputLayout dropInd
    ){
        Exercise ex = new Exercise();
        ex.setName(name.getEditText().getText().toString());
        ex.setWeight(Integer.parseInt(weight.getEditText().getText().toString()));
        ex.setDropSet_rate(Integer.parseInt(dropInd.getEditText().getText().toString()));
        return ex;
    }

    /**
     * Registration of one-set oneSet
     */
    public ExerciseSet registerOneSet() {
        // Return new Exercise Set for one set
        return this.buildExercise(
                this.inputName1,
                this.inputSets1,
                this.inputRepts1,
                this.inputWeight1,
                this.inputResting,
                this.inputDropInd1
        );
    }


    /**
     * Registration of bi-set oneSet
     */
    public ExerciseSet registerBiSet() {

        /* Builds bi-set object */
        ExerciseSet exerciseSet = new ExerciseSet(2);
        exerciseSet.setSeries(
                new Series(
                        Integer.parseInt(this.inputSets1.getEditText().getText().toString()),
                        Integer.parseInt(this.inputRepts1.getEditText().getText().toString())
                )
        );

        exerciseSet.setRest_timing(Integer.parseInt(this.inputResting.getEditText().getText().toString()));

        exerciseSet.setExercise(
                0,
                this.buildExercise(
                      this.inputName1,
                      this.inputWeight1,
                      this.inputDropInd1
                )
        );

        exerciseSet.setExercise(
                1,
                this.buildExercise(
                        this.inputName2,
                        this.inputWeight2,
                        this.inputDropInd2
                )
        );

        return exerciseSet;
    }


    /**
     * Registration of tri-set oneSet
     */
    public ExerciseSet registerTriSet() {

        /* Builds bi-set object */
        ExerciseSet exerciseSet = new ExerciseSet(3);
        exerciseSet.setSeries(
                new Series(
                        Integer.parseInt(this.inputSets1.getEditText().getText().toString()),
                        Integer.parseInt(this.inputRepts1.getEditText().getText().toString())
                )
        );

        exerciseSet.setRest_timing(Integer.parseInt(this.inputResting.getEditText().getText().toString()));

        exerciseSet.setExercise(
                0,
                this.buildExercise(
                        this.inputName1,
                        this.inputWeight1,
                        this.inputDropInd1
                )
        );

        exerciseSet.setExercise(
                1,
                this.buildExercise(
                        this.inputName2,
                        this.inputWeight2,
                        this.inputDropInd2
                )
        );

        exerciseSet.setExercise(
                2,
                this.buildExercise(
                        this.inputName3,
                        this.inputWeight3,
                        this.inputDropInd3
                )
        );

        return exerciseSet;
    }



    /**
     * Configs the registration.
     *
     * Firstly, determinate which radio button was checked.
     *  -> One-set hides bi-set and tri-set views;
     *  -> Bi-set hides tri-set views;
     *
     * After this, sets a listener for save button click.
     *  Validates the form, according to the level (One-set, bi-set or tri-set ):
     *      -> Bi-set includes One-set;
     *      -> Tri-set includes One-set and Bi-set.
     *
     * @param checkedId id of which button was checked
     */
    public void configExerciseRegistration(final int checkedId) {

        final int level;

        /* If is one set oneSet */
        if(checkedId == R.id.exerciseRegs_radio_oneSet) {
            hideTriSet(); /* Hide tri-set views */
            hideBiSet();  /* Hide bi-set views */
            level = 1;

        }

        /* If is bi set oneSet */
        else if(checkedId == R.id.exerciseRegs_radio_biSet) {
            showBiSet();
            hideTriSet(); /* Hide tri-set views */
            level = 2;
        }

        /* Else, if is tri set oneSet */
        else if(checkedId == R.id.exerciseRegs_radio_triSet){
            showTriSet();
            level = 3;
        }

        else level = 1;

        this.btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // If form isn't valid, then do nothing.
                if (!validateForm(level)) return;

                // If form is valid and
                if(level == 1)  exerciseSet = registerOneSet();
                else if(level == 2) exerciseSet = registerBiSet();
                else if(level == 3) exerciseSet = registerTriSet();
                else exerciseSet = null;


                if (exerciseSet != null) {
                    int moduleId = getModuleID();
                    if (moduleId != -1)
                        dataPasser.onExerciseRegistered(exerciseSet, moduleId);

                    // Exclude this fragment
                    getActivity()
                            .getSupportFragmentManager()
                            .beginTransaction()
                            .remove(ExerciseRegisterFragment.this)
                            .commit();
                }

                else {
                    Toast.makeText(
                            getContext(),
                            "Erro ao cadastrar exercício",
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }
        });
    }


    public int getModuleID() {
        char module = getArguments().getChar("Module");

        if (module == 'A') return 0;
        if (module == 'B') return 1;
        if (module == 'C') return 2;
        if (module == 'D') return 3;
        if (module == 'E') return 4;
        if (module == 'F') return 5;
        return -1;
    }

}
