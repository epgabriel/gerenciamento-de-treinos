package ekelf.bodygymapp.fragment.register.training;

import ekelf.bodygymapp.model.enums.TrainingModuleType;
import ekelf.bodygymapp.model.exercise.ExerciseSet;
import ekelf.bodygymapp.model.exercise.WarmingUp;


/**
 * TrainingModuleType Registrable interface.
 * This interface defines methods to data passer between training register activity and its fragments.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-04-10
 */
public interface TrainingRegistrable {

    /**
     * Event triggered when user is found in the database.
     * @param key String
     */
    void onUserFind(String key, TrainingModuleType trainingModuleType);


    /**
     * Event triggered when user confirms a training module.
     */
    void onTrainingModuleDefined();


    /**
     * Event triggered when user register an exercise Set
     * @param exerciseSet
     * @param id
     */
    void onExerciseRegistered(ExerciseSet exerciseSet, int id);


    /**
     * Event triggered when user register a warming-up exercise.
     * @param warmingUp
     * @param id
     */
    void onWarmingUpRegistered(WarmingUp warmingUp, int id);


    /**
     * Event triggered when user register a pos-training exercise.
     * @param posTraining
     * @param id
     */
    void onPosTrainingRegistered(WarmingUp posTraining, int id);


    /**
     * Event triggered when user register a training module description.
     * @param description
     * @param id
     */
    void onDescriptionDefined(String description, int id);


    /**
     * Event triggered whe a module register was finish.
     * It might pass to the another module or finalize the training registration.
     */
    void onModuleSet();

}
