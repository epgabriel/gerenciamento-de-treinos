package ekelf.bodygymapp.fragment.register.training;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.activity.MainActivity;
import ekelf.bodygymapp.fragment.register.Registrable;
import ekelf.bodygymapp.model.actors.User;
import ekelf.bodygymapp.model.enums.TrainingModuleType;
import ekelf.bodygymapp.persistence.UserDAO;

/**
 * A simple {@link Fragment} subclass.
 *
 * @author Gabriel Edmilson Pinto
 * @version 1.0
 * @since 2019-04-10
 */
public class TrainingRegisterInitialConfigFragment extends Fragment implements Registrable {

    /* Initial Config */
    private TrainingModuleType trainingModuleType = TrainingModuleType.AB ;
    private TrainingRegistrable dataPasser;
    private TextInputLayout input_userID;
    private ProgressBar progressBar;

    public TrainingRegisterInitialConfigFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataPasser = (TrainingRegistrable) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View initialConfig = inflater.inflate(R.layout.fragment_training_register_initial_config, container, false);



        input_userID = initialConfig.findViewById(R.id.TrainingRegisterFragInitial_textInputLayout_userID);



        // Config progress bar.
        progressBar  = initialConfig.findViewById(R.id.TrainingRegisterFragInitial_progressBar);
        hideProgressBar();



        // Config radio group for training modules
        ((RadioGroup) initialConfig.findViewById(R.id.TrainingRegisterFragInitial_radioGroupModels))
                .setOnCheckedChangeListener(modelCheckOnCheckedChangeListener());



        // Set onClick Listener for Next and Back Buttons.
        initialConfig.findViewById(R.id.TrainingRegisterFragInitial_btn_next).setOnClickListener(checkUserOnClickListener());
        initialConfig.findViewById(R.id.TrainingRegisterFragInitial_btn_back).setOnClickListener(backClickListener());



        return initialConfig;
    }


    /** @return listener for radio button model change */
    private RadioGroup.OnCheckedChangeListener modelCheckOnCheckedChangeListener() {
        return new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.TrainingRegisterFragInitial_radioGroupModels_AB) trainingModuleType = TrainingModuleType.AB;
                else if (checkedId == R.id.TrainingRegisterFragInitial_radioGroupModels_ABC) trainingModuleType = TrainingModuleType.ABC;
                else if (checkedId == R.id.TrainingRegisterFragInitial_radioGroupModels_ABCD) trainingModuleType = TrainingModuleType.ABCD;
                else trainingModuleType = TrainingModuleType.ABCDE;
            }
        };
    }


    /** @return listener for next button on click */
    private View.OnClickListener checkUserOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validForm()) return;
                showProgressBar();
                new UserDAO()
                        .queryRegistration()
                        .equalTo(Integer.parseInt(input_userID.getEditText().getText().toString()))
                        .addListenerForSingleValueEvent(checkUserExistenceListener());
            }
        };
    }


    /**
     * Checks the user existence with the given id. If so, then goes to the next step.
     * Else, then finalize with failure.
     * @return database listener to check if the user exists
     */
    private ValueEventListener checkUserExistenceListener() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // If the query has results
                if (dataSnapshot.hasChildren()) {

                    // User exists, so retrieve its key
                    String key = dataSnapshot
                            .getChildren()
                            .iterator()
                            .next()
                            .getKey();

                    // if key exists, then respond data to the activity
                    if(key != null) {
                        dataPasser.onUserFind(key, trainingModuleType);
                        dataPasser.onTrainingModuleDefined();
                        hideProgressBar();
                    }

                    // An error occurred on trying to retrieve user's key.
                    else finalizeWithFailure(getContext().getResources().getString(R.string.error_retrieving_user_data));
                }

                // There was no user found with the given ID.
                else finalizeWithFailure(getContext().getResources().getString(R.string.error_user_not_found));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                finalizeWithFailure(databaseError.toString());
            }
        };
    }


    /** @return listener for back button on click */
    private View.OnClickListener backClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UserDAO().getReference().child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                User user = dataSnapshot.getValue(User.class);
                                Intent intent = new Intent(getContext(), MainActivity.class);
                                intent.putExtra("user", user);
                                startActivity(intent);
                                getFragmentManager().beginTransaction()
                                        .remove(TrainingRegisterInitialConfigFragment.this).commit();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                getActivity().onBackPressed();
                                getFragmentManager().beginTransaction()
                                        .remove(TrainingRegisterInitialConfigFragment.this).commit();
                            }
                        });
            }
        };
    }

    /** Hides progress bar. */
    private void hideProgressBar() { this.progressBar.setVisibility(View.GONE); }

    /** Shows progress bar. */
    private void showProgressBar() { this.progressBar.setVisibility(View.VISIBLE); }



    @Override
    public void finalizeNotFilled() {
        Toast.makeText(
                getContext(),
                getContext().getResources().getString(R.string.error_fields_are_missing),
                Toast.LENGTH_SHORT
        ).show();
        hideProgressBar();
    }



    @Override
    public void finalizeWithSuccess() { hideProgressBar(); }



    @Override
    public void finalizeWithFailure(String e) {
        Toast.makeText(getContext(), e, Toast.LENGTH_SHORT).show();
        hideProgressBar();
    }



    @Override
    public boolean validForm() {
        boolean filled = true;

        // Verify if the user ID input was not given.
        if(this.input_userID.getEditText().getText().toString().isEmpty()) {
            filled = false;
            this.input_userID.setErrorEnabled(true);
            this.input_userID.setError(getContext().getResources().getString(R.string.error_required));
        }

        // Else, then the data was given, clear error notification.
        else this.input_userID.setError(null);


        return filled;
    }
}
