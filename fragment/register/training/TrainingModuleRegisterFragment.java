package ekelf.bodygymapp.fragment.register.training;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ekelf.bodygymapp.R;
import ekelf.bodygymapp.adapter.AdapterWarmingUp;
import ekelf.bodygymapp.adapter.AdapterWorkoutLog;
import ekelf.bodygymapp.model.exercise.ExerciseSet;
import ekelf.bodygymapp.model.exercise.WarmingUp;
import ekelf.bodygymapp.model.training.TrainingModule;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainingModuleRegisterFragment extends Fragment {

    // Recycler views
    private RecyclerView rv_warmingUp, rv_exercise, rv_posTraining;

    private List<ExerciseSet> exerciseSet;

    private WarmingUp warmingUp, posTraining;

    private TrainingRegistrable dataPasser;

    private TextView tv_title;

    private TextInputLayout input_description;


    // Required empty public constructor
    public TrainingModuleRegisterFragment() {}


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.dataPasser = (TrainingRegistrable) context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View trainingRegisterModuleView = inflater.inflate(R.layout.fragment_training_module_register, container, false);

        this.input_description = trainingRegisterModuleView.findViewById(R.id.trainingModuleRegister_input_description);
        this.tv_title = trainingRegisterModuleView.findViewById(R.id.trainingModuleRegister_textView_title);

        // Initial variables
        if (getArguments().getSerializable("TrainingModule") != null){
            this.exerciseSet = ((TrainingModule)getArguments().getSerializable("TrainingModule")).getExerciseSetList();
            this.warmingUp = ((TrainingModule)getArguments().getSerializable("TrainingModule")).getWarmingUp();
            this.posTraining = ((TrainingModule)getArguments().getSerializable("TrainingModule")).getPosTraining();
            this.input_description.getEditText().setText(((TrainingModule)getArguments().getSerializable("TrainingModule")).getDescription());
            this.tv_title.setText("Cadastro do Módulo " + getArguments().getChar("Module"));
        }else {
            this.exerciseSet = new ArrayList<>();
            this.warmingUp = null;
            this.posTraining = null;
            this.tv_title.setText("Cadastro de Módulo de Treinamento");
        }



        // Recycler view setting
        this.rv_warmingUp   = trainingRegisterModuleView.findViewById(R.id.trainingModuleRegister_recyclerView_warmingUp);
        this.rv_warmingUp.setVisibility(View.GONE);

        this.rv_exercise    = trainingRegisterModuleView.findViewById(R.id.trainingModuleRegister_recyclerView_exercise);
        this.rv_exercise.setVisibility(View.GONE);

        this.rv_posTraining = trainingRegisterModuleView.findViewById(R.id.trainingModuleRegister_recyclerView_posTraining);
        this.rv_posTraining.setVisibility(View.GONE);


        // Config Warming-up recycler view
        if (this.warmingUp != null) {
            this.rv_warmingUp.setVisibility(View.VISIBLE);
            this.rv_warmingUp.setLayoutManager(new LinearLayoutManager(getContext()));
            this.rv_warmingUp.setHasFixedSize(true);
            this.rv_warmingUp.setAdapter(new AdapterWarmingUp(warmingUp));
        }

        // Config OneSet recycler view
        if (!this.exerciseSet.isEmpty()) {
            this.rv_exercise.setVisibility(View.VISIBLE);
            this.rv_exercise.setLayoutManager(new LinearLayoutManager(getContext()));
            this.rv_exercise.setHasFixedSize(true);
            this.rv_exercise.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
            this.rv_exercise.setAdapter(new AdapterWorkoutLog(exerciseSet));
        }



        if (this.posTraining != null) {
            this.rv_posTraining.setVisibility(View.VISIBLE);
            this.rv_posTraining.setLayoutManager(new LinearLayoutManager(getContext()));
            this.rv_posTraining.setHasFixedSize(true);
            this.rv_posTraining.setAdapter(new AdapterWarmingUp(posTraining));
        }



        // Set click listener for Warming Up add.
        trainingRegisterModuleView.findViewById(R.id.trainingModuleRegister_btn_add_warmingUp)
                .setOnClickListener(addWarmingUpOnClickListener());



        // Set click listener for OneSet add.
        trainingRegisterModuleView.findViewById(R.id.trainingModuleRegister_btn_add_exercise)
                .setOnClickListener(addExerciseOnClickListener());



        // Set click listener for Pos Training add.
        trainingRegisterModuleView.findViewById(R.id.trainingModuleRegister_btn_add_posTraining)
                .setOnClickListener(addPosTrainingOnClickListener());


        // Set click listener for button next
        trainingRegisterModuleView.findViewById(R.id.trainingModuleRegister_btn_next)
                .setOnClickListener(nextOnClickListener());


        // Set click listener for button back
        trainingRegisterModuleView.findViewById(R.id.trainingModuleRegister_btn_back)
                .setOnClickListener(backOnClickListener());



        return trainingRegisterModuleView;
    }


    /**
     * Triggered event for warming up exercise add.
     * @return
     */
    private View.OnClickListener addWarmingUpOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validateForm()) return;

                Bundle bundle = new Bundle();
                bundle.putChar("Module", getArguments().getChar("Module"));
                bundle.putInt("warming", 0);

                Fragment frag = new WarmingUpRegisterFragment();
                frag.setArguments(bundle);

                getFragmentManager().beginTransaction()
                        .replace(R.id.TrainingRegisterAct_CL, frag)
                        .commit();
            }
        };
    }


    /**
     * Triggered event for exercise add.
     * @return
     */
    private View.OnClickListener addExerciseOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validateForm()) return;

                Bundle bundle = new Bundle();
                bundle.putChar("Module", getArguments().getChar("Module"));

                Fragment frag = new ExerciseRegisterFragment();
                frag.setArguments(bundle);

                getFragmentManager().beginTransaction()
                        .replace(R.id.TrainingRegisterAct_CL, frag)
                        .commit();


            }
        };
    }


    /**
     * Triggered event for pos training exercise.
     * @return
     */
    private View.OnClickListener addPosTrainingOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validateForm()) return;

                Bundle bundle = new Bundle();
                bundle.putChar("Module", getArguments().getChar("Module"));
                bundle.putInt("warming", 1);

                Fragment frag = new WarmingUpRegisterFragment();
                frag.setArguments(bundle);

                getFragmentManager().beginTransaction()
                        .replace(R.id.TrainingRegisterAct_CL, frag)
                        .commit();
            }
        };
    }



    /**
     * Triggered event for next button.
     * @return
     */
    private View.OnClickListener nextOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validateForm()) return;
                dataPasser.onModuleSet();
                getFragmentManager().beginTransaction()
                        .remove(TrainingModuleRegisterFragment.this)
                        .commit();
            }
        };
    }



    /**
     * Triggered event for back button.
     * @return
     */
    private View.OnClickListener backOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        };
    }


    public boolean validateForm() {
        boolean valid = true;

        if(this.input_description.getEditText().getText().toString().isEmpty()) {
            valid = false;
            this.input_description.setErrorEnabled(true);
            this.input_description.setError(getContext().getResources().getString(R.string.error_required));
        }

        else{
            this.input_description.setError(null);
            dataPasser.onDescriptionDefined(this.input_description.getEditText().getText().toString(), getModuleID());
        }


        return valid;
    }


    public int getModuleID() {
        char module = getArguments().getChar("Module");

        if (module == 'A') return 0;
        if (module == 'B') return 1;
        if (module == 'C') return 2;
        if (module == 'D') return 3;
        if (module == 'E') return 4;
        if (module == 'F') return 5;
        return -1;
    }

}
